FROM python:3.6.5

# Install node prereqs, nodejs and yarn
# Ref: https://deb.nodesource.com/setup_10.x
# Ref: https://yarnpkg.com/en/docs/install
RUN \
  apt-get update && \
  apt-get install -yqq apt-transport-https
RUN \
  echo "deb https://deb.nodesource.com/node_10.x stretch main" > /etc/apt/sources.list.d/nodesource.list && \
  wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
  wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  apt-get update && \
  apt-get install -yqq nodejs yarn && \
  pip install -U pip && pip install pipenv && \
  npm i -g npm@^6 && \
  rm -rf /var/lib/apt/lists/*

# Set up react app
RUN npm install pm2 -g
ENV REACT_APP_API_BASE_URL http://localhost:5000

# Set up postman tests
RUN npm install newman -g

# Set up Flask app
ENV FLASK_APP main.py
ENV DATABASE_URI postgresql://master:3l3ph&nt!@endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com:5432/endangerousDB
ENV SCHEMA_NAME endangerous
RUN apt-get update
RUN apt-get -y install libboost-all-dev
RUN apt-get -y install libgmp-dev
RUN apt-get -y install vim
RUN pip install --upgrade pip
RUN pip --version
RUN pip install coverage
RUN pip install pylint
RUN pip install Flask-Restless
RUN pip install flask_sqlalchemy

# Set up backend tests
RUN pip install requests
RUN pip install psycopg2

# Set up selenium
RUN pip install selenium
RUN apt-get update
RUN apt-get install unzip
RUN apt-get update
RUN apt-get -y install xvfb

ARG CHROME_VERSION="google-chrome-stable"
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  && apt-get -qqy install \
    ${CHROME_VERSION:-google-chrome-stable} \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

ARG CHROME_DRIVER_VERSION="latest"
RUN CD_VERSION=$(if [ ${CHROME_DRIVER_VERSION:-latest} = "latest" ]; then echo $(wget -qO- https://chromedriver.storage.googleapis.com/LATEST_RELEASE); else echo $CHROME_DRIVER_VERSION; fi) \
&& echo "Using chromedriver version: "$CD_VERSION \
&& wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CD_VERSION/chromedriver_linux64.zip \
&& rm -rf /opt/selenium/chromedriver \
&& unzip /tmp/chromedriver_linux64.zip -d /opt/selenium \
&& rm /tmp/chromedriver_linux64.zip \
&& mv /opt/selenium/chromedriver /opt/selenium/chromedriver-$CD_VERSION \
&& chmod 755 /opt/selenium/chromedriver-$CD_VERSION \
&& ln -fs /opt/selenium/chromedriver-$CD_VERSION /usr/bin/chromedriver


CMD [ "bash" ]

