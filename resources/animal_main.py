import requests
import xml.etree.ElementTree as ET
import re
from animal_handler import animal_handler
from animal_database import animal_database


def extract_list(f_name):
    l = []
    f = open(f_name)
    for line in f:
        l.append(line[0: len(line) - 1])
    f.close()
    return l

def write_to_db(name, is_animal, handler):
    resp = requests.get(name_search_url + '?name=' + name + '&NSAccessKeyId=' + key)
    root = ET.fromstring(resp.content)
    listResults = root[1]

    infos = []
    for child in listResults:
        print child.attrib["uid"]
        infos.append(handler.handle_id(child.attrib["uid"], is_animal))
        print ""
    
    db = animal_database("host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!")
    db.connect()

    for info in infos:
        db.write(info)
    
    db.disconnect()

if __name__ == "__main__":
    name_search_url = "https://services.natureserve.org/idd/rest/ns/v1/globalSpecies/list/nameSearch"
    key = "a8924d1e-2748-47f8-84cc-4013da0e5e8b"
    files = [
        './amphibians.txt'
    ]
    handler = animal_handler()

    for f_name in files:
        index = 0
        animals = extract_list(f_name)
        is_animal = False if f_name == "./plants.txt" else True
        for ani in animals:
            write_to_db(ani, is_animal, handler)
            index += 1
