import requests
import urllib
import psycopg2
conn = None



def executeQuery(cursor, name, population):
    sql = "UPDATE endangerous.states set human_pop=%s WHERE name=%s"
    cursor.execute(sql, (population, name))
    return cursor.mogrify(sql, (population, name))

def buildUrl(num):
    url_population = "https://api.census.gov/data/2017/pep/population?get=POP,GEONAME&DATE=9"
    param = "&for=state:"+str(num)
    url = url_population + param
    return url

try:
    conn = psycopg2.connect("host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!")
    cur = conn.cursor()

    for num in range(2, 57):
        r = requests.get(buildUrl(num))
        state_dict = {}
        if(r.status_code==200):
            data = r.json()[1]
            executeQuery(cur, data[1], data[0])

    conn.commit()
    cur.close()
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()
