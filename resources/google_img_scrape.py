from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import time
import psycopg2
from psycopg2.extensions import AsIs

def get_src(sci_name, browser):
    browser.get('https://www.google.com/')
    search_form = browser.find_element_by_name('q')
    search_form.send_keys(sci_name)
    search_form.submit()
    element = WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "qs"))
        )

    xpath = "//a[text()=\"Images\"]"
    browser.find_element_by_xpath(xpath).click()

    element = WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "rg_ic"))
        )

    img_thumb = browser.find_elements_by_class_name("rg_ic")
    img_elem = None
    for elem in img_thumb:
        if elem.get_attribute("id") != "":
            img_elem = elem
            break

    print img_elem.get_attribute("id")
    img_elem.click()

    element = WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "irc_mi"))
        )
    time.sleep(1)
    big_img = browser.find_elements_by_class_name("irc_mi")

    results = [elem.get_attribute("src") for elem in big_img]
    for src in results:
        if src != None:
            return src 
    
    browser.save_screenshot('debug.png')
    assert False

def write_to_db(srcs):
    host_name = "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
    conn = psycopg2.connect(host_name)
    cur = conn.cursor()

    for l in srcs:
        print l
        print l[1]
        link = l[1].replace("'", "''")
        name = l[0].replace("'", "''")
        sql = "UPDATE endangerous.animals SET image_link='" + link + "' WHERE common_name='" + name + "'"
        cur.execute(sql)
    
    conn.commit()
    cur.close()
    conn.close() 


opts = Options()
opts.set_headless()
assert opts.headless  # Operating in headless mode
browser = Firefox(options=opts)
f_name = "amphibian_common_names.txt"
f = open(f_name)

srcs = []
try:
    for line in f:
        line = line[0:len(line)-1]
        print "Trying " + line
        src = get_src(line, browser)
        srcs.append((line, src))
        print src + "\n"
except Exception as e:
    browser.save_screenshot('debug.png')
    print e
    print e.__class__
finally:
    print "\n\n-------- WRITING TO DB --------\n\n"
    write_to_db(srcs)
    browser.quit()
