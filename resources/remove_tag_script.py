import requests
import urllib
import psycopg2
import re
conn = None


def executeQuery(cursor):
    sqlSelect = "SELECT plant_id, description, threats_notes, category_reason, category_comment from endangerous.plants WHERE plant_id="
    sqlUpdate = "UPDATE endangerous.plants set description=%s, threats_notes=%s,  category_reason=%s, category_comment=%s WHERE plant_id="
    for num in range(1, 1572):
        select = sqlSelect+str(num)
        update = sqlUpdate+str(num)
        cursor.execute(select)
        #print(cursor.mogrify(sqlSelect))
        #for document in cursor:
        document = cursor.fetchone()
        if document is not None:
            #print(document)
            print(num)
            threat = remove_html_tags(document[2])
            description = remove_html_tags(document[1])
            reason = remove_html_tags(document[3])
            comment = remove_html_tags(document[4])
            id = document[0]
            cursor.execute(update, (description, threat, reason, comment))


def remove_html_tags(text):
    """Remove html tags from a string"""
    if text is None:
        return ""
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)

try:
    conn = psycopg2.connect("host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!")
    cur = conn.cursor()

    executeQuery(cur)

    conn.commit()
    cur.close()
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()