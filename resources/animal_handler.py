import requests
import xml.etree.ElementTree as ET
import re

class animal_handler:
    def __init__(self):
        self.url = "https://services.natureserve.org/idd/rest/ns/v1.1/globalSpecies/comprehensive?"
        self.key = "a8924d1e-2748-47f8-84cc-4013da0e5e8b"
        self.animal_count = 1001
        self.plant_count = 1178

    def handle_id(self, id, is_animal):
        resp = requests.get(self.url + "NSAccessKeyId=" + self.key + "&uid=" + id)
        root = ET.fromstring(resp.content)
        parsed = self.parse_response(root)
        
        if is_animal:
            parsed["animal_id"] = self.animal_count
            self.animal_count += 1
        else:
            parsed["plant_id"] = self.plant_count
            self.plant_count += 1
        # print parsed
        return parsed
    
    def parse_response(self, root):
        parsed = {}
        # All responses surrounded in an outer tag
        root = root[0]
        # API is very inconsistent, the order of XML elems is different for some
        child_dict = self.get_child_dict(root)

        for tag in child_dict:
            child = root[child_dict[tag]]
            if tag == "classification":
                parsed.update(self.handle_classification(child))
            elif tag == "conservationStatus":
                parsed.update(self.handle_conservation(child))
            elif tag == "distribution":
                parsed.update(self.handle_distribution(child))
            elif tag == "ecologyAndLifeHistory":
                parsed.update(self.handle_ecology_and_life_history(child))
        
        return parsed

    # Maps from XML elem name to the position in the tree
    def get_child_dict(self, root):
        child_dict = {}
        index = 0
        for child in root:
            tag = re.sub(r'\{[^()]*\}', '', child.tag)
            child_dict[tag] = index
            index += 1
        return child_dict
    
    def handle_classification(self, classification):
        return_dict = {}

        # We are assuming that the classificiation tag does not change from animal to animal
        if len(classification[0]) >= 2:
            common_name = classification[0][1].text
            return_dict['common_name'] = common_name
        else:
            return_dict['common_name'] = None


        sci_name = classification[0][0][0].text
        return_dict['sci_name'] = sci_name

        kingdom = classification[1][0][0].text
        return_dict['tax_kingdom'] = kingdom

        phylum = classification[1][0][1].text
        return_dict['tax_phylum'] = phylum

        tax_class = classification[1][0][2].text
        return_dict['tax_class'] = tax_class

        order = classification[1][0][3].text
        return_dict['tax_order'] = order

        family = classification[1][0][4].text
        return_dict['tax_family'] = family

        genus = classification[1][0][5].text
        return_dict['tax_genus'] = genus

        return return_dict

    def handle_conservation(self, conservation):
        conservation = conservation[0][0]
        return_dict = {}
        child_dict = self.get_child_dict(conservation)

        for child in child_dict:
            index = child_dict[child]
            if child == "roundedRank":
                category = conservation[index][1].text
                return_dict["category"] = category
            elif child == "reasons":
                reason = conservation[index].text
                return_dict["category_reason"] = reason
            elif child == "conservationStatusFactors":
                csf = self.get_conservation_status_factors(conservation[index])
                return_dict.update(csf)
        
        return return_dict

    def get_conservation_status_factors(self, base):
        child_dict = self.get_child_dict(base)
        return_dict = {}
        return_dict["category_comment"] = None
        return_dict["threats_notes"] = None

        for child in child_dict:
            index = child_dict[child]
            if child == "globalAbundance":
                comment = ""
                if len(base[index]) > 3:
                    comment = base[index][3].text
                    return_dict["category_comment"] = comment
            elif child == "threat":
                threat_notes = base[index][0].text
                return_dict["threats_notes"] = threat_notes

        return return_dict

    def handle_distribution(self, distribution):
        child_dict = self.get_child_dict(distribution)
        states = None
        provinces = None

        for child in child_dict:
            index = child_dict[child]
            if child == "nations":
                index2 = 0
                for nations in distribution[index]:
                    if nations.attrib["nationName"] == "United States":
                        states = self.extract_territories(distribution[child_dict[child]][index2])
                    elif nations.attrib["nationName"] == "Canada":
                        provinces = self.extract_territories(distribution[child_dict[child]][index2])
                    index2 += 1
        return_dict = {'states': states, 'provinces': provinces}
        return return_dict

    def extract_territories(self, nation):
        territories = []
        child_dict = self.get_child_dict(nation)

        for child in child_dict:
            index = child_dict[child]
            if child == "subnations":
                subnations = nation[index]
                for subnation in subnations:
                    territories.append(subnation.attrib["subnationCode"])
    
        return territories
        
    def handle_ecology_and_life_history(self, ealh):
        return_dict = {}
        child_dict = self.get_child_dict(ealh)
        return_dict["habitats"] = None
        return_dict['description'] = None

        for child in child_dict:
            if child == "ecologyAndLifeHistoryDescription":
                description = ""
                if len(ealh[child_dict[child]]) < 2:
                    description = ealh[child_dict[child]][0].text
                else:
                    description = ealh[child_dict[child]][1].text
                return_dict['description'] = description
            elif child == "habitats":
                habitats = self.extract_habitats(ealh[child_dict[child]])
                return_dict["habitats"] = habitats
        
        return return_dict
    
    def extract_habitats(self, hab):
        habitats = []
        child_dict = self.get_child_dict(hab)
        for child in child_dict:
            if child.endswith("Habitats"):
                index = child_dict[child]
                for habitat in hab[index]:
                    habitats.append(habitat.text)
        return habitats        
