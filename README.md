# Living Endangerously

Internet database for animal and habitat conservation information.  

Members:
Maya Kothare-Arora - mk35362, mkarora  
Kendall Bowen - klb4448, kbowen  
Jack Stenglein - jms9652, jackstenglein  
Joe Vilches - jsv549, joevilches  
Terrell Watson - tw25776, TWatson98  

Git SHA: 8970d7a18e2f88828caa4d7ff12746f7b0ff5565  

GitLab Pipelines: https://gitlab.com/joevilches/living-endangerously/pipelines  

Website: https://www.livingendangerously.me/

Estimated Completion Times:  
Maya: 10  
Kendall: 10  
Jack: 10  
Joe: 10  
Terrell: 10  

Actual Completion Times:  
Maya: 12  
Kendall: 12  
Jack: 11  
Joe: 12  
Terrell: 10  

Comments: We have frontend/src/index.js instead of frontend/index.js
