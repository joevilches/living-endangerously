import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.css';

// Routing
import { BrowserRouter, Route } from 'react-router-dom';

// Our components
import App from './App';
import About from './about/About';
import AnimalModel from './models/AnimalModel';
import AnimalInstance from './instances/AnimalInstance';
import PlantModel from './models/PlantModel';
import StateModel from './models/StateModel';
import ResultModel from './models/ResultModel';
import PlantInstance from './instances/PlantInstance';
import StateInstance from './instances/StateInstance';
import Visualization from './visualization/Visualization'

// CSS
import './index.css';

ReactDOM.render((
    <BrowserRouter>
        <div>
            <Route exact path="/" component={App} />
            <Route path="/about" component={About} />
            <Route exact path="/animals" component={AnimalModel} />
            <Route exact path="/results" component={ResultModel}/>
            <Route exact path="/plants" component={PlantModel} />
            <Route exact path="/states" component={StateModel} />
            <Route path="/animals/:id" component={AnimalInstance} />
            <Route path="/plants/:id" component={PlantInstance} />
            <Route path="/states/:id" component={StateInstance} />
            <Route path="/visualization" component={Visualization} />
        </div>
    </BrowserRouter>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
