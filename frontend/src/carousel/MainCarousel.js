import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel'

import './MainCarousel.css'

class MainCarousel extends Component {
    render() {
        /* return(
            <Carousel id="carousel">
                <Carousel.Item>
                    <img
                    id="slide-img"
                    src="../../polar-bear.jpg"
                    alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>Explore Animals</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    id="slide-img"
                    src="../../forest-canopy.jpg"
                    alt="Second slide"
                    />
                    <Carousel.Caption>
                        <h3>Explore Plants</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    id="slide-img"
                    src="../../cali-condor.jpg"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <h3>States and Provinces</h3>
                    <p>Discover what governments are doing to help.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        ); */
        return (
            <div>
                <img src="../../kemps.jpg" id="first-img" alt="turtle"/>
                <div id="first-text">
                    There are more than <span id="endanger-count">50,438</span> endangered <br /> plants and animals
                    in the United States and Canada.
                </div>
            </div>
        );
    }
}

export default MainCarousel;