import React from 'react';
import { Fade } from 'react-slideshow-image';
import { Link } from 'react-router-dom';
import './MainCarousel.css';


const fadeProperties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: false,
  indicators: true,
  autoplay: false
};


function OrganismCarousel(props) {
    if (props.organisms === undefined) {
        return null;
    }

    const carouselItems = [];
    props.organisms.forEach(function(organism, i) {
        carouselItems.push(
            <div key={i} className="each-fade">
                <div className="image-container">
                    <Link to={props.linkBase.concat(organism.id)}>
                        <img src={organism.image_link} alt={organism.common_name} />
                    </Link>
                </div>
                <div className="description-container">
                    <Link to={props.linkBase.concat(organism.id)}>
                        <h4>{organism.common_name}</h4>
                    </Link>
                    <h5>{organism.category}</h5>
                    <p>{organism.description}</p>
                </div>
          </div>
        )
    });

    return(
      <div className="organismCarousel">
        <Fade {...fadeProperties}>
            {carouselItems}
        </Fade>
      </div>

    );
}

export default OrganismCarousel;
