import React from 'react';

function Error(props) {

    return (
        <div id="error" className="container-fluid modelContainer">
            <h3 className="text-center">Ouch!</h3>
            <h4 className="text-center">It looks like something you did broke our site. Please stop doing it.</h4>
        </div>
    );
}

export default Error;