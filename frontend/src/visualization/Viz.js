import React from 'react';
import HeatMap from './HeatMap';
import './Visualization.css';
import 'react-d3-components';
import CumulativePieChart from './CumulativePieChart';

function loadStateProtectedData(states) {
    var data = [[]];
    for (let i = 0; i < states.length; ++i) {
        data[i] = [states[i].code, states[i].name, 100 - states[i].unprotected_int];
    }
    return data;
}

function loadStateDensityData(states) {
    var data = [[]];
    for(let i = 0; i < states.length; ++i) {
        var miles = states[i].land_area_int/640;
        var density = Math.floor( (states[i].human_pop/miles) );
        data[i]= [states[i].code, states[i].name, density];
    }
    return data;
}

function removeZeroValues(statistics) {
    for (let i = 0; i < statistics.length; ++i) {
        if (statistics[i].value === 0) {
            statistics.splice(i, 1);
            --i;
        }
    }
}

function loadStateOrganismCounts(states) {
    var data = [[]];
    for (let i = 0; i < states.length; ++i) {
        const obj = states[i];
        const extinct = obj.extinct_wild_animals + obj.extinct_wild_plants + obj.possibly_extinct_animals + obj.possibly_extinct_plants + obj.presumed_extinct_animals + obj.presumed_extinct_plants;
        const imperiled = obj.critically_imperiled_animals + obj.critically_imperiled_plants + obj.imperiled_animals + obj.imperiled_plants;
        const vulnerable = obj.vulnerable_animals + obj.vulnerable_plants;
        data[i] = [obj.code, obj.name, extinct + imperiled + vulnerable];

    }
    return data;
}

function Viz(props) {

    if (!props.states) {
        return null;
    }

    removeZeroValues(props.animalStatistics);
    removeZeroValues(props.plantStatistics);

    return (
        <div>
            <div className="row">
                <div className="col">
                    <div>
                        <h1 className="visual-header">Protected Land by State</h1>
                        <HeatMap width={null} height={null} tooltipPrepend="Percent Protected: " tooltipAppend="%"
                                 legendAppend=" percent" id="protected-land-heat-map"
                                 data={loadStateProtectedData(props.states)} color="#1f4f33"/>
                    </div>
                </div>
                <div className="col">
                    <div>
                        <h1 className="visual-header">Population Density by State</h1>
                        <HeatMap width={null} height={null} tooltipPrepend="Population Density: "
                                 tooltipAppend=" people per sq. mile" legendAppend=" people per sq. mile"
                                 id="pop-density-heat-map" data={loadStateDensityData(props.states)} color="#ba5300"/>
                    </div>
                </div>
                <div className="col">
                    <div>
                        <h1 className="visual-header">Organisms Rated Vulnerable or Worse</h1>
                        <HeatMap width={null} height={null} tooltipPrepend="" tooltipAppend=" organisms"
                                 legendAppend=" organisms" id="pop-density-heat-map"
                                 data={loadStateOrganismCounts(props.states)} color="#7f0000"/>
                    </div>
                </div>
            </div>
            <br />
            <div className="row">
                {
                    props.animalStatistics &&
                    (
                        <div className="col-6" style={{height: 700}}>
                            <h1 className="visual-header">Animal Breakdown</h1>
                            <CumulativePieChart data={props.animalStatistics}/>
                        </div>
                    )
                }
                {
                    props.plantStatistics &&
                    (
                        <div className="col-6" style={{height: 700}}>
                            <h1 className="visual-header">Plant Breakdown</h1>
                            <CumulativePieChart data={props.plantStatistics}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
}

export default Viz;
