import React, { Component } from 'react';
import MainNavbar from '../navbar/MainNavbar';
import './Visualization.css';
import Viz from './Viz';
import ProvViz from './ProvViz';


class Visualization extends Component{
    constructor(props) {
        super(props);
        this.state = {
            tab: "le",
            objects: null,
            isLoaded: false,
            error: null,
            medianIncomes: null,
            salaries: null,
            stateProtectedData: null,
            stateDensityData: null,
            adToGrad: null,
            animalsLoaded: false,
            animalStatistics: null,
            plantStatistics: null
        }
        //set up state
    }

    getObjects() {
        const apiBaseURL = process.env.REACT_APP_API_BASE_URL || "https://api.livingendangerously.me";
        var url = apiBaseURL.concat("/api/").concat("states").concat("?results_per_page=50&page=1");

        fetch(url)
            .then(function(res) {
                if (!res.ok) {
                    throw Error(res.statusText);
                }
                return res.json();
            }).then((result) => {
                this.setState({
                    objects: result.objects
                });
                window.scrollTo(0, 0);
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                this.setState({
                    isLoaded: true,
                    error: error,
                });
            }
        );
    }

    getConservationStatistics(model) {
        const conservationStatistics = [
            {name: 'Secure', value: 0},
            {name: 'Apparently Secure', value: 0},
            {name: 'Vulnerable', value: 0},
            {name: 'Imperiled', value: 0},
            {name: 'Critically Imperiled', value: 0},
            {name: 'Possibly Extinct', value: 0},
            {name: 'Presumed Extinct', value: 0},
            {name: 'Unrankable', value: 0},
            {name: 'Not Yet Ranked', value: 0},
            {name: 'Not Applicable', value: 0}
        ];

        const promises = [];
        conservationStatistics.forEach(function(categoryDict, index) {
            promises.push(new Promise(function(resolve, reject) {
                const apiBaseURL = process.env.REACT_APP_API_BASE_URL || "https://api.livingendangerously.me";
                var url = apiBaseURL.concat('/api/').concat(model)
                    .concat('?results_per_page=1&q={"filters":[{"name":"category","op":"like","val":"').concat(categoryDict.name).concat('"}]}');
                fetch(url).then(function(res) {
                    if (!res.ok) {
                        throw Error(res.statusText);
                    }
                    return res.json();
                }).then((result) => {
                    resolve({name: categoryDict.name, value: result.num_results});
                },
                (error) => {
                    reject();
                });
            }));

        });

        return Promise.all(promises);
    }

    componentDidMount() {
        this.getObjects();
        this.getConservationStatistics("animals").then((animalStatistics) => {
            this.setState({animalStatistics: animalStatistics});
        });
        this.getConservationStatistics("plants").then((plantStatistics) => {
            this.setState({plantStatistics: plantStatistics});
        });
        this.getMedianIncomeByState();
        this.getSalaryByMajor();
        this.loadAdmissionToGradData();
    }

    loadAdmissionToGradData(){
        const url = "https://api.myfuture.education/universities?columns=graduation_rate,admission_rate,school_name&order_by=admission_rate";
        fetch(url).then(function(res) {
            if (!res.ok) {
                throw Error(res.statusText);
            }
            return res.json();
        }).then((result) => {
                const adToGrad = [];
                result.data.forEach(function(uni) {
                    var uniObj = {x: Math.round(uni.admission_rate*10000)/100, y: Math.round(uni.graduation_rate*10000)/100, z: uni.school_name};
                    adToGrad.push(uniObj);
                });
                this.setState({adToGrad: adToGrad})
            },
            (error) => {
                this.setState({error: error});
            });
    }

    getMedianIncomeByState() {
        const url = "https://api.myfuture.education/cities";
        fetch(url).then(function(res) {
            if (!res.ok) {
                throw Error(res.statusText);
            }
            return res.json();
        }).then((result) => {
            const medianIncomesDict = {};
            result.data.forEach(function(city) {
                if (city.abbrv_state in medianIncomesDict) {
                    medianIncomesDict[city.abbrv_state].count++;
                    medianIncomesDict[city.abbrv_state].total_income += city.income;
                } else {
                    let state = {name: city.state, count: 1, total_income: city.income};
                    medianIncomesDict[city.abbrv_state] = state;
                }
            });

            const medianIncomes = [];
            Object.keys(medianIncomesDict).forEach(function(state) {
                medianIncomes.push([state, medianIncomesDict[state].name, Math.round(medianIncomesDict[state].total_income * 100.0 / medianIncomesDict[state].count) / 100]);
            });
            this.setState({medianIncomes: medianIncomes});
        },
        (error) => {
            this.setState({error: error});
        });
    }

    formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    getSalaryByMajor() {
        const url = "https://api.myfuture.education/majors";
        fetch(url).then(function(res) {
            if (!res.ok) {
                throw Error(res.statusText);
            }
            return res.json();
        }).then((result) => {
            const salaries = [];
            result.data.forEach(function(major) {
              salaries.push({label: major.major, value: Math.ceil(major.starting_salary/100)*100});
            });

            this.setState({salaries: salaries});
        },
        (error) => {
            this.setState({error: error});
        });
    }

    loadStateProtectedData() {
        var obj = this.state.objects;
        var data = [[]];
        for (let i = 0; i < obj.length; ++i) {
            var state = [];
            state[0] = obj[i].code;
            state[1] = obj[i].name;
            state[2] = 100 - obj[i].unprotected_int;
            data[i] = state;
        }
        return data;
    }

    loadStateDensityData() {
        var obj = this.state.objects;
        var i;
        var data = [[]];
        for(i = 0; i<obj.length; i++){
            var state = [];
            state[0] = obj[i].code;
            var miles = obj[i].land_area_int/640;
            var density = Math.round( (obj[i].human_pop/miles)*10 )/10;
            state[1] = obj[i].name;
            state[2] = density;
            data[i]=state;
        }

        return data;
    }


    render() {
        if (!this.state.objects || !this.state.animalStatistics || !this.state.plantStatistics) {
            return (
                <div>
                    <MainNavbar from="about-page"/>
                    <div className="lds-circle" style={{marginLeft: '45%'}}>
                        <div></div>
                    </div>
                </div>
            );
        }

        if (this.state.error) {
            console.log("Error: ", this.state.error);
        }

        this.state.stateProtectedData = this.loadStateProtectedData();
        this.state.stateDensityData = this.loadStateDensityData();

        return (<div id="visualization-page">
            <MainNavbar from="visual-page"/>
            <div className="visHeader">
                <img src="../../earth.jpg" className="visHeaderPicture"/>
                <h1 className="header-text">See the Big Picture</h1>
                <span className="header-text-sub">Data Visualization</span>
                <ul id="selector">
                    <li id={this.state.tab === "le" ? "active" : null} class="selector-elem" onClick={() => this.selectTab("le")}>Our Visualizations</li>
                    <li id={this.state.tab === "prov" ? "active" : null} class="selector-elem" onClick={() => this.selectTab("prov")}>Provider Visualizations</li>
                </ul>
                <div class="tab-wrapper">
                    {this.state.tab === "le" ? <Viz states={this.state.objects} animalStatistics={this.state.animalStatistics} plantStatistics={this.state.plantStatistics}/> : null}
                    {this.state.tab === "prov" ? <ProvViz medianIncomes={this.state.medianIncomes} salaries={this.state.salaries} adToGrad={this.state.adToGrad}/> : null}
                </div>
            </div>
            </div>
        );
    }

    selectTab(val) {
        this.setState({tab : val});
    }

}

export default Visualization;
