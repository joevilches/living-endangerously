import React, { Component } from 'react';
import HeatMap from './HeatMap';
import GapBubbleChart from './GapBubbleChart';
import './Visualization.css';
import 'react-d3-components'
import {
   ScatterChart, CartesianGrid, XAxis, YAxis, Tooltip, Scatter, Label
} from 'recharts';

class ProvViz extends Component {

    render() {
        const CustomTooltip = ({ active, payload, label }) => {
            console.log(payload);
            if (active) {
                return (
                    <div className="custom-tooltip" style={{background:'#d9e5f7'}}>
                        <p className="label">{payload[0].payload.z}</p>
                        <p className="label">{`${payload[0].name} : ${payload[0].value}${payload[0].unit}`}</p>
                        <p className="label">{`${payload[1].name} : ${payload[1].value}${payload[0].unit}`}</p>

                    </div>
                );
            }
            return null
        };

      return(
        <div>
        <br />
        {
            this.props.medianIncomes &&
            (
                <div>
                    <h3 class="visual-header">Average Income by State</h3>
                    <HeatMap tooltipPrepend="Average Income: $" tooltipAppend="" legendPrepend="$" id="income-heat-map" data={this.props.medianIncomes} color="#0c5e00"/>
                </div>
            )
        }
        <br />
        <div className="bubble-map">
            <h3 className="visual-header">Starting Salary by Major</h3>
            <GapBubbleChart data={this.props.salaries}/>
        </div>
            <br />
            <div className="scatter-chart">
                <h3 className="visual-header">Admission Rate vs. Graduation Rate</h3>
            <ScatterChart
                className="graduation-rate-scatter"
                width={400}
                height={400}
                margin={{
                    top: 20, right: 20, bottom: 20, left: 20,
                }}
            >
                <CartesianGrid />
                <XAxis type="number" dataKey="x" name="Admission Rate" unit="%">
                    <Label value="Admission Rate" offset={0} position="bottom" />
                </XAxis>
                <YAxis type="number" dataKey="y" name="Graduation Rate" unit="%"
                       label={{ value: 'Graduation Rate', angle: -90, position: 'insideLeft' }}/>
                <Tooltip viewBox content={<CustomTooltip />} />
                <Scatter name="Admission Rate vs. Graduation Rate" data={this.props.adToGrad}  fill="#8884d8" />
            </ScatterChart>
            </div>
        </div>

      );
    }
  }
  export default ProvViz;
