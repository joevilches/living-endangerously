import React, { Component } from 'react';
import * as d3 from "d3";
import USAMap from '../usa-map/USAMap.js';


class HeatMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            paletteScale: null,
            minValue: 0,
            maxValue: 0
        };
        this.heatMapConfig = this.heatMapConfig.bind(this);
    }


    componentDidMount() {
        let onlyValues = this.props.data.map(function (obj) { return obj[2]; });
        let minValue = Math.min.apply(null, onlyValues), maxValue = Math.max.apply(null, onlyValues);
        this.setState({minValue: minValue, maxValue: maxValue});
    }

    mapHandler(event) {
        // Nothing to do, but this function is required
    }

    heatMapConfig() {
        var paletteScale = d3.scaleLinear()
            .domain([this.state.minValue, this.state.maxValue])
            .range(["#EFEFFF", this.props.color]);
        var config = {};
        this.props.data.forEach(function(item) {
            config[item[0]] = {
                fill: paletteScale(item[2]),
                title: item[1] + "\n" + this.props.tooltipPrepend + item[2] + this.props.tooltipAppend
            };
        }, this);
        return config;
    }

    render() {
        return (
            <div>
                <USAMap width={this.props.width} height={this.props.height} title="" customize={this.heatMapConfig()} onClick={this.mapHandler} />
                <br />

                <div className="legend-row">
                    <svg width="14" height="14" viewBox="0 0 32 32">
                        <path stroke="none" fill={this.props.color} d="M0,4h32v24h-32z"/>
                    </svg>
                    <span className="legend-text">{this.props.legendPrepend}{this.state.maxValue}{this.props.legendAppend}</span>
                </div>
                <div className="legend-row">
                    <svg width="14" height="14" viewBox="0 0 32 32">
                        <path stroke="none" fill="#EFEFFF" d="M0,4h32v24h-32z"/>
                    </svg>
                    <span className="legend-text">{this.props.legendPrepend}{this.state.minValue}{this.props.legendAppend}</span>
                </div>
            </div>
        );
    }
}

export default HeatMap;