import React from 'react';
import {
    ResponsiveContainer, PieChart, Pie, Legend, Cell,
} from 'recharts';

const COLORS = {
    'Secure': '#5fcc9c',
    'Apparently Secure': '#699380',
    'Vulnerable': '#e99d30',
    'Imperiled': '#f0ee41',
    'Critically Imperiled': '#99a021',
    'Extinct in the Wild': '#c70039',
    'Possibly Extinct': '#f46942',
    'Presumed Extinct': '#a615ab',
    'Unrankable': '#b0bec1',
    'Not Yet Ranked': '#09194f',
    'Not Applicable': '#000000'
};

function CumulativePieChart(props) {
    return (
        <div>
            <ResponsiveContainer width="100%" height={430}>
                <PieChart>
                    <Pie
                        data={props.data}
                        outerRadius={150}
                        fill="#8884d8"
                        paddingAngle={4}
                        minAngle={2}
                        dataKey="value"
                        label
                    >
                        {
                            props.data.map(function(entry, index) {
                                return (<Cell key={`cell-${index}`} fill={COLORS[entry.name]} />);
                            })
                        }
                    </Pie>
                    <Legend verticalAlign="bottom"/>
                </PieChart>
            </ResponsiveContainer>
        </div>
    );
}

export default CumulativePieChart;
