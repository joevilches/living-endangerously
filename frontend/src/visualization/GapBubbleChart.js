import React from 'react';
import BubbleChart from '@weknow/react-bubble-chart-d3';

function GapBubbleChart(props) {
    return (
        <BubbleChart
            graph={{
                zoom: 1, // 1.1 means 110% of zoom.
                offsetX: 0, // -0.05 means that the offset is -5% of the graph width.
                offsetY: 0
            }}
            legendPercentage={25}
            width={800}
            height={800}
            viewBox="0 0 750 800"
            fontFamily="Arial"
            data={props.data}
        />
    );
}

export default GapBubbleChart;
