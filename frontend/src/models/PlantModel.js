import React, { Component } from 'react';
import OrganismCard from './cards/OrganismCard';
import Paginator from './Paginator.js';
import queryString from 'query-string';
import MainNavbar from '../navbar/MainNavbar';
import PlantFilter from './filter/PlantFilter.js';
const sortFields = [
    {'name': 'Common Name', 'key': 'common_name'},
    {'name': 'Scientific Name', 'key': 'sci_name'},
    {'name': 'Conservation Category', 'key': 'category'}
];
const searchFields = ['common_name', 'sci_name', 'category', 'description', 'threats_notes', 'category_reason', 'category_comment'];

class PlantModel extends Component {
    render() {
        const values = queryString.parse(this.props.location.search);
        var page = 1;
        if (values.page !== undefined) {
            page = values.page;
        }
        return (
            <div>
                <MainNavbar from="plant-page"/>
                <Paginator
                    cardType={OrganismCard}
                    filterType={PlantFilter}
                    currentPage={page}
                    modelName="plants"
                    sortFields={sortFields}
                    searchFields={searchFields}
                    query={values.query}
                />
            </div>
        );
    }
}

export default PlantModel;
