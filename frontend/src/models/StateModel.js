import React, { Component } from 'react';
import StateFilter from './filter/StateFilter.js';
import StateCard from './cards/StateCard';
import Paginator from './Paginator.js';
import queryString from 'query-string';
import MainNavbar from '../navbar/MainNavbar';

const sortFields = [
    {'name': 'Name', 'key': 'name'},
    {'name': 'Human Population', 'key': 'human_pop_int'},
    {'name': 'Land Area', 'key': 'land_area_int'},
    {'name': 'GAP 1 Land', 'key': 'gap1_int'},
    {'name': 'GAP 2 Land', 'key': 'gap2_int'},
    {'name': 'GAP 3 Land', 'key': 'gap3_int'},
    {'name': 'Unprotected Land', 'key': 'unprotected_int'},
];
const searchFields = ['name', 'code'];

class StateModel extends Component {
    render() {
        const values = queryString.parse(this.props.location.search);
        var page = 1;
        if (values.page !== undefined) {
            page = values.page;
        }

        return (
            <div>
               <MainNavbar from="state-page"/> 
                <Paginator
                    cardType={StateCard}
                    filterType={StateFilter}
                    currentPage={page}
                    modelName="states"
                    sortFields={sortFields}
                    searchFields={searchFields}
                    query={values.query}
                />
            </div>
        );
    }
  }

export default StateModel;
