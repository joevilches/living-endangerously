import React, { Component } from 'react';
import {Form, Col, Button, ButtonToolbar, Collapse} from 'react-bootstrap';
import { MdExpandMore, MdChevronRight } from 'react-icons/md';

class OrganismFilter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: {
                "Secure": false,
                "Apparently Secure": false,
                "Vulnerable": false,
                "Imperiled": false,
                "Critically Imperiled": false,
                "Extinct in the Wild": false,
                "Possibly Extinct": false,
                "Presumed Extinct": false,
                "Not Yet Ranked": false,
                "Unrankable": false
            },
            states: [
                {"name": "Alabama", "code": "AL", "value": false},
                {"name": "Alaska", "code": "AK", "value": false},
                {"name": "Arizona", "code": "AZ", "value": false},
                {"name": "Arkansas", "code": "AR", "value": false},
                {"name": "California", "code": "CA", "value": false},
                {"name": "Colorado", "code": "CO", "value": false},
                {"name": "Connecticut", "code": "CT", "value": false},
                {"name": "Delaware", "code": "DE", "value": false},
                {"name": "Florida", "code": "FL", "value": false},
                {"name": "Georgia", "code": "GA", "value": false},
                {"name": "Hawaii", "code": "HI", "value": false},
                {"name": "Idaho", "code": "ID", "value": false},
                {"name": "Illinois", "code": "IL", "value": false},
                {"name": "Indiana", "code": "IN", "value": false},
                {"name": "Iowa", "code": "IA", "value": false},
                {"name": "Kansas", "code": "KS", "value": false},
                {"name": "Kentucky", "code": "KY", "value": false},
                {"name": "Louisiana", "code": "LA", "value": false},
                {"name": "Maine", "code": "ME", "value": false},
                {"name": "Maryland", "code": "MD", "value": false},
                {"name": "Massachusetts", "code": "MA", "value": false},
                {"name": "Michigan", "code": "MI", "value": false},
                {"name": "Minnesota", "code": "MN", "value": false},
                {"name": "Mississippi", "code": "MS", "value": false},
                {"name": "Missouri", "code": "MO", "value": false},
                {"name": "Montana", "code": "MT", "value": false},
                {"name": "Nebraska", "code": "NE", "value": false},
                {"name": "Nevada", "code": "NV", "value": false},
                {"name": "New Hampshire", "code": "NH", "value": false},
                {"name": "New Jersey", "code": "NJ", "value": false},
                {"name": "New Mexico", "code": "NM", "value": false},
                {"name": "New York", "code": "NY", "value": false},
                {"name": "North Carolina", "code": "NC", "value": false},
                {"name": "North Dakota", "code": "ND", "value": false},
                {"name": "Ohio", "code": "OH", "value": false},
                {"name": "Oklahoma", "code": "OK", "value": false},
                {"name": "Oregon", "code": "OR", "value": false},
                {"name": "Pennsylvania", "code": "PA", "value": false},
                {"name": "Rhode Island", "code": "RI", "value": false},
                {"name": "South Carolina", "code": "SC", "value": false},
                {"name": "South Dakota", "code": "SD", "value": false},
                {"name": "Tennessee", "code": "TN", "value": false},
                {"name": "Texas", "code": "TX", "value": false},
                {"name": "Utah", "code": "UT", "value": false},
                {"name": "Vermont", "code": "VT", "value": false},
                {"name": "Virginia", "code": "VA", "value": false},
                {"name": "Washington", "code": "WA", "value": false},
                {"name": "West Virginia", "code": "WV", "value": false},
                {"name": "Wisconsin", "code": "WI", "value": false},
                {"name": "Wyoming",	"code": "WY", "value": false}
            ],
            habitats: {
                "Alpine": false,
                "Barrens": false,
                "Big River": false,
                "Bog/fen": false,
                "Cliff": false,
                "Creek": false,
                "Cropland/Hedgerow": false,
                "Desert": false,
                "Forest - Conifer": false,
                "Forest Edge": false,
                "Forested Wetland": false,
                "Forest - Hardwood": false,
                "Forest - Mixed": false,
                "Forest/Woodland": false,
                "Grassland": false,
                "Herbaceous Wetland": false,
                "High Gradient": false,
                "Low Gradient": false,
                "Medium River": false,
                "Near Shore": false,
                "Old Field": false,
                "Playa/Salt Flat": false,
                "Riparian": false,
                "Sand/Dune": false,
                "Savanna": false,
                "Scrub-Shrub Wetland": false,
                "Shallow Water": false,
                "Shrubland/Chaparral": false,
                "Spring/Spring Brook": false,
                "Talus/Scree": false,
                "Temporary Pool": false,
                "Tundra": false,
                "Unknown": false,
                "Urban/Edificarian": false,
                "Woodland - Conifer": false,
                "Woodland - Hardwood": false,
                "Woodland - Mixed": false
            },
            categoriesApplied: false,
            statesApplied: false,
            habitatsApplied: false,
            categoriesOpen: false,
            statesOpen: false,
            habitatsOpen: false
        };
        // Bind functions for category
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.removeCategoryFilter = this.removeCategoryFilter.bind(this);
        this.handleCategorySubmit = this.handleCategorySubmit.bind(this);

        // Bind functions for state
        this.onStateChange = this.onStateChange.bind(this);
        this.handleStateSubmit = this.handleStateSubmit.bind(this);
        this.removeStateFilter = this.removeStateFilter.bind(this);

        // Bind functions for habitats
        this.onHabitatChange = this.onHabitatChange.bind(this);
        this.handleHabitatSubmit = this.handleHabitatSubmit.bind(this);
        this.removeHabitatFilter = this.removeHabitatFilter.bind(this);
    }

    onCategoryChange(category) {
        return (function onChange() {
            let categories = this.state.categories;
            categories[category] = !categories[category];
            this.setState({"categories": categories});
        }).bind(this);
    }

    handleCategorySubmit() {
        let selected = false;
        for (let key in this.state.categories) {
            if (this.state.categories[key]) {
                selected = true;
                break;
            }
        }

        if (!selected) {
            this.removeCategoryFilter();
        } else {
            const filters = [{
                "or": []
            }];

            for (let key in this.state.categories) {
                if (this.state.categories[key]) {
                    filters[0].or.push({
                        "name": "category",
                        "op": "eq",
                        "val": key
                    });
                }
            }

            this.props.apply("category", filters);
            this.setState({categoriesApplied: true});
        }
    }

    removeCategoryFilter() {
        this.props.remove("category");
        let categories = this.state.categories;
        for (let key in categories) {
            categories[key] = false;
        }

        this.setState({categoriesApplied: false, categories: categories});
    }

    onStateChange(index) {
        return (function onChange() {
            let states = this.state.states;
            states[index].value = !states[index].value;
            this.setState({states: states});
        }).bind(this);
    }

    handleStateSubmit() {
        let selected = false;
        for (let i = 0; i < this.state.states.length; i++) {
            if (this.state.states[i].value) {
                selected = true;
                break;
            }
        }

        if (!selected) {
            this.removeStateFilter();
        } else {
            const filters = [{
                "or": []
            }];

            this.state.states.forEach(function(state) {
                if (state.value) {
                    filters[0].or.push({
                        "name": "states__code",
                        "op": "any",
                        "val": state.code
                    });
                }
            })

            this.props.apply("states__code", filters);
            this.setState({statesApplied: true});
        }

    }

    removeStateFilter() {
        this.props.remove("states__code");
        let states = this.state.states;
        for (let i in states) {
            states[i].value = false;
        }
        this.setState({statesApplied: false, states: states});
    }

    onHabitatChange(name) {
        return (function onChange() {
            let habitats = this.state.habitats;
            habitats[name] = !habitats[name];
            this.setState({"habitats": habitats});
        }).bind(this);
    }

    handleHabitatSubmit() {
        let selected = false;
        for (let key in this.state.habitats) {
            if (this.state.habitats[key]) {
                selected = true;
                break;
            }
        }

        if (!selected) {
            this.removeHabitatFilter();
        } else {
            const filters = [{
                "or": []
            }];

            for (let key in this.state.habitats) {
                if (this.state.habitats[key]) {
                    filters[0].or.push({
                        "name": "habitats__habitat_name",
                        "op": "any",
                        "val": key
                    });
                }
            }

            this.props.apply("habitats__habitat_name", filters);
            this.setState({habitatsApplied: true});
        }
    }

    removeHabitatFilter() {
        this.props.remove("habitats__habitat_name");
        let habitats = this.state.habitats;
        for (let key in habitats) {
            habitats[key] = false;
        }
        this.setState({habitatsApplied: false, habitats: habitats});
    }


    render() {

        // Build the state checkbox columns
        const statesCol1 = [];
        var i = 0;
        for (; i < this.state.states.length / 2; i++) {
            statesCol1.push(<Form.Check key={i} type="checkbox" checked={this.state.states[i].value} label={this.state.states[i].name} onChange={this.onStateChange(i)}/>);
        }

        const statesCol2 = [];
        for (; i < this.state.states.length; i++) {
            statesCol2.push(<Form.Check key={i} type="checkbox" checked={this.state.states[i].value} label={this.state.states[i].name} onChange={this.onStateChange(i)}/>);
        }

        // Build the habitat checkbox columns
        i = 0;
        const habitatEntries = Object.entries(this.state.habitats);
        const habitatsCol1 = [];
        for (; i < habitatEntries.length / 2; i++) {
            const [name, value] = habitatEntries[i];
            habitatsCol1.push(<Form.Check key={name} type="checkbox" checked={value} label={name} onChange={this.onHabitatChange(name)}/>)
        }

        const habitatsCol2 = [];
        for (; i < habitatEntries.length; i++) {
            const [name, value] = habitatEntries[i];
            habitatsCol2.push(<Form.Check key={name} type="checkbox" checked={value} label={name} onChange={this.onHabitatChange(name)}/>)
        }

        const categoryChevron = this.state.categoriesOpen ? MdExpandMore : MdChevronRight;
        const stateChevron = this.state.statesOpen ? MdExpandMore : MdChevronRight;
        const habitatChevron = this.state.habitatsOpen ? MdExpandMore : MdChevronRight;

        // Actually render the component
        return (
            <div>
                <div className="row m-0">
                    <div className="col m-b-15">
                        <h3>Filter Results</h3>
                    </div>
                </div>
                <div className="row m-0">
                    <div className="col">
                        <div className="row align-items-center m-0" onClick={()=>this.setState({categoriesOpen: !this.state.categoriesOpen})}>
                            <h3 className="col-1 p-0 mb-0">{React.createElement(categoryChevron, null, null)}</h3>
                            <h5 className="col-11 pr-0 mb-0">Conservation Category</h5>
                        </div>
                            <Form className={this.state.categoriesOpen ? "collapse-form" : "collapse-form-hidden"}>
                                <Collapse in={this.state.categoriesOpen}><div>
                                    <Form.Row className="state-filter-row">
                                        <Col>
                                            {
                                                Object.entries(this.state.categories).map(function([name, value]) {
                                                    return (<Form.Check key={name} type="checkbox" checked={value} label={name} onChange={this.onCategoryChange(name)}/>);
                                                }, this)
                                            }
                                        </Col>
                                    </Form.Row>
                                    <Form.Row>
                                        <Col>
                                            <ButtonToolbar>
                                                <Button variant="primary" onClick={this.handleCategorySubmit}>Apply</Button>
                                                {
                                                    this.state.categoriesApplied && (
                                                        <Button variant="danger" onClick={this.removeCategoryFilter} className="m-l-10">Reset</Button>
                                                    )
                                                }
                                            </ButtonToolbar>
                                        </Col>
                                    </Form.Row>
                                </div></Collapse>
                            </Form>
                    </div>
                </div>
                <hr />
                <div className="row m-0">
                    <div className="col">
                        <div className="row align-items-center m-0" onClick={()=>this.setState({statesOpen: !this.state.statesOpen})}>
                            <h3 className="col-1 p-0 mb-0">{React.createElement(stateChevron, null, null)}</h3>
                            <h5 className="col-11 pr-0 mb-0">State</h5>
                        </div>
                        <Form className={this.state.statesOpen ? "collapse-form" : "collapse-form-hidden"}>
                            <Collapse in={this.state.statesOpen}><div>
                                <Form.Row className="state-filter-row">
                                    <Col>
                                        { statesCol1 }
                                    </Col>
                                    <Col>
                                        { statesCol2 }
                                    </Col>
                                </Form.Row>
                                <Form.Row>
                                    <Col>
                                        <ButtonToolbar>
                                            <Button variant="primary" onClick={this.handleStateSubmit}>Apply</Button>
                                            {
                                                this.state.statesApplied && (
                                                    <Button variant="danger" onClick={this.removeStateFilter} className="m-l-10">Reset</Button>
                                                )
                                            }
                                        </ButtonToolbar>
                                    </Col>
                                </Form.Row>
                            </div></Collapse>
                        </Form>
                    </div>
                </div>
                <hr />
                <div className="row m-0">
                    <div className="col pr-0">
                        <div className="row align-items-center m-0" onClick={()=>this.setState({habitatsOpen: !this.state.habitatsOpen})}>
                            <h3 className="col-1 p-0 mb-0">{React.createElement(habitatChevron, null, null)}</h3>
                            <h5 className="col-11 pr-0 mb-0">Habitat</h5>
                        </div>
                        <Form className="collapse-form">
                            <Collapse in={this.state.habitatsOpen}><div>
                                <Form.Row className="state-filter-row">
                                    <Col>
                                        { habitatsCol1 }
                                    </Col>
                                    <Col className="pr-0">
                                        { habitatsCol2 }
                                    </Col>
                                </Form.Row>
                                <Form.Row>
                                    <Col>
                                        <ButtonToolbar>
                                            <Button variant="primary" onClick={this.handleHabitatSubmit}>Apply</Button>
                                            {
                                                this.state.habitatsApplied && (
                                                    <Button variant="danger" onClick={this.removeHabitatFilter} className="m-l-10">Reset</Button>
                                                )
                                            }
                                        </ButtonToolbar>
                                    </Col>
                                </Form.Row>
                            </div></Collapse>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}

export default OrganismFilter;
