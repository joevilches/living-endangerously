import React from 'react';
import StateFilterSection from './StateFilterSection.js';


function StateFilter(props) {
    return (
        <div>
            <div className="row m-0">
                <div className="col m-b-15">
                    <h3>Filter Results</h3>
                </div>
            </div>

            <div className="row m-0">
                <StateFilterSection title="Human Population" apply={props.apply} remove={props.remove} fieldName="human_pop_int"/>
            </div>
            <hr />
            <div className="row m-0">
                <StateFilterSection title="Total Land Area" apply={props.apply} remove={props.remove} fieldName="land_area_int" />
            </div>
            <hr />
            <div className="row m-0">
                <StateFilterSection title="GAP 1 Area" apply={props.apply} remove={props.remove} fieldName="gap1_int" />
            </div>
            <hr />
            <div className="row m-0">
                <StateFilterSection title="GAP 2 Area" apply={props.apply} remove={props.remove} fieldName="gap2_int" />
            </div>
            <hr />
            <div className="row m-0">
                <StateFilterSection title="GAP 3 Area" apply={props.apply} remove={props.remove} fieldName="gap3_int" />
            </div>
            <hr />
            <div className="row m-0">
                <StateFilterSection title="Unprotected Area" apply={props.apply} remove={props.remove} fieldName="unprotected_int" />
            </div>
            <br />
        </div>
    );
}

export default StateFilter;
