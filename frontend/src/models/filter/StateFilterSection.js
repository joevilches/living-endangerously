import React, { Component } from 'react';
import {Form, Col, ButtonToolbar, Button} from 'react-bootstrap';
import './Filter.css';


class StateFilterSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            minimum: "",
            maximum: "",
            minimumError: null,
            maximumError: null,
            applied: false
        };
        this.handleMinimumChange = this.handleMinimumChange.bind(this);
        this.handleMaximumChange = this.handleMaximumChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
    }

    handleMinimumChange(event) {
        this.setState({minimum: event.target.value});
    }

    handleMaximumChange(event) {
        this.setState({maximum: event.target.value});
    }

    // Prevent the user from typing non-numerical characters
    handleKeyPress(event) {
        if (event.key < '0' || event.key > '9') {
            event.preventDefault();
        }
    }

    handleSubmit() {
        let min = parseInt(this.state.minimum);
        let max = parseInt(this.state.maximum);

        if (isNaN(min) || isNaN(max)) {
            this.setState({
                minimumError: isNaN(min) ? 'Please enter a number.' : null,
                maximumError: isNaN(max) ? 'Please enter a number.' : null
            });
        } else if (max < min) {
            this.setState({
                minimumError: null,
                maximumError: 'Max must be greater than or equal to min.'
            });
        } else {
            this.setState({
                minimumError: null,
                maximumError: null,
                applied: true
            });

            const filters = [
                {
                    "name": this.props.fieldName,
                    "op": "ge",
                    "val": min
                },
                {
                    "name": this.props.fieldName,
                    "op": "le",
                    "val": max
                }
            ];
            this.props.apply(this.props.fieldName, filters);
        }
    }

    removeFilter() {
        this.props.remove(this.props.fieldName);
        this.setState({applied: false, minimum: "", maximum: ""});
    }

    render() {

        let minInvalid = (this.state.minimumError != null);
        let maxInvalid = (this.state.maximumError != null);

        return (
            <div className="col">
                <h5>{this.props.title}</h5>
                <Form>
                    <Form.Row className="state-filter-row">
                        <Col>
                            <Form.Control value={this.state.minimum} placeholder="Minimum" onKeyPress={this.handleKeyPress} onChange={this.handleMinimumChange} isInvalid={minInvalid}/>
                            <Form.Control.Feedback type="invalid" >
                                {this.state.minimumError}
                            </Form.Control.Feedback>
                        </Col>
                        <Col>
                            <Form.Control value={this.state.maximum} placeholder="Maximum" onKeyPress={this.handleKeyPress} onChange={this.handleMaximumChange} isInvalid={maxInvalid}/>
                            <Form.Control.Feedback type="invalid">
                                {this.state.maximumError}
                            </Form.Control.Feedback>
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <ButtonToolbar>
                                <Button variant="primary" onClick={this.handleSubmit}>Apply</Button>
                                {
                                    this.state.applied && (
                                        <Button variant="danger" onClick={this.removeFilter} className="m-l-10">Reset</Button>
                                    )
                                }
                            </ButtonToolbar>
                        </Col>
                    </Form.Row>
                </Form>
            </div>
        )
    }
}

export default StateFilterSection;