import React, { Component } from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import OrganismFilter from './filter/OrganismFilter.js';
import OrganismCard from './cards/OrganismCard';
import Paginator from './Paginator.js';
import queryString from 'query-string';
import StateFilter from './filter/StateFilter.js';
import StateCard from "./cards/StateCard";
import MainNavbar from '../navbar/MainNavbar';


const organismSortFields = [
    {'name': 'Common Name', 'key': 'common_name'},
    {'name': 'Scientific Name', 'key': 'sci_name'},
    {'name': 'Conservation Category', 'key': 'category'}
];

const stateSortFields = [
    {'name': 'Name', 'key': 'name'},
    {'name': 'Human Population', 'key': 'human_pop_int'},
    {'name': 'Land Area', 'key': 'land_area_int'},
    {'name': 'GAP 1 Land', 'key': 'gap1_int'},
    {'name': 'GAP 2 Land', 'key': 'gap2_int'},
    {'name': 'GAP 3 Land', 'key': 'gap3_int'},
    {'name': 'Unprotected Land', 'key': 'unprotected_int'},
];

const stateFields = ['name', 'code'];
const organismFields = ['common_name', 'sci_name', 'category', 'description', 'threats_notes', 'category_reason', 'category_comment'];


class ResultModel extends Component {

    constructor(props) {
        super(props);
        this.state = {key: 'animals'};
    }

    getQuery(searchList, fields) {
        var query = {"filters": [{"or": []}]};
        for (const search in searchList) {
            for (const field of fields) {
                query.filters[0].or.push({
                    "name": field,
                    "op": "ilike",
                    "val": "%25".concat(searchList[search]).concat("%25")
                });
            }
        }
        return query;
    }


    render() {
        const values = queryString.parse(this.props.location.search);
        const searchStr = (values.search === undefined) ? "" : values.search;
        const searchList = searchStr.split(" ");
        const stateQuery = this.getQuery(searchList, stateFields);
        const organismQuery = this.getQuery(searchList, organismFields);

        return (
            <div>
                <MainNavbar />
                <div className="container-fluid">
                    <h4 id="result-search-text">Results for "{searchStr}":</h4>
                    <div id="result-page-loaded">
                        <Tabs id="controlled-tabs" activeKey ={this.state.key} onSelect={key => this.setState({key})}>
                            <Tab eventKey="animals" title="Animals">
                                <Paginator
                                    cardType={OrganismCard}
                                    filterType={OrganismFilter}
                                    currentPage={1}
                                    modelName="animals"
                                    sortFields={organismSortFields}
                                    query={JSON.stringify(organismQuery)}
                                    searchList={searchList}
                                />
                            </Tab>
                            <Tab eventKey="plants" title="Plants">
                                <Paginator
                                    cardType={OrganismCard}
                                    filterType={OrganismFilter}
                                    currentPage={1}
                                    modelName="plants"
                                    sortFields={organismSortFields}
                                    query={JSON.stringify(organismQuery)}
                                    searchList={searchList}
                                />
                            </Tab>
                            <Tab eventKey="states" title="States">
                                <Paginator
                                    cardType={StateCard}
                                    filterType={StateFilter}
                                    currentPage={1}
                                    modelName="states"
                                    sortFields={stateSortFields}
                                    query={JSON.stringify(stateQuery)}
                                    searchList={searchList}
                                />
                            </Tab>
                        </Tabs>
                    </div>
                </div>
            </div>
        );
    }
}

export default ResultModel;
