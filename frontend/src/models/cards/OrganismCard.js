import React from 'react';
import { Link } from 'react-router-dom';
import './OrganismCard.css';
import Highlighter from "react-highlight-words";

// We are going to display the common_name, scientific_name, category, pop_trend, image, territories

const snippetCategories = ['description', 'threats_notes', 'category_reason', 'category_comment'];

function getTerritoryString(territories) {
    var result = "";
    const separator = ", ";
    var i = 0;
    for (; i < territories.length && i < 5; i++) {
        result = result.concat(territories[i].code).concat(separator);
    }

    if (result.length === 0) {
        return "Unknown";
    }

    if (i < territories.length) {
        return result.concat("...");
    } else {
        return result.substring(0, result.length - separator.length);
    }
}

function getHabitatString(habitats) {
    var result = "";
    const separator = ", ";
    habitats.forEach(function(habitat) {
        result = result.concat(habitat.habitat_name).concat(separator);
    });

    return result.substring(0, result.length - separator.length);
}

function getSnippet(fullText, searchList) {
    if (fullText === null || fullText === undefined
        || searchList === null || searchList === undefined ) {
        return null;
    }

    for (let i = 0; i < searchList.length; i++) {
        const termIndex = fullText.toLowerCase().indexOf(searchList[i].toLowerCase());
        if (termIndex > -1) {
            const sentenceBegin = Math.max(fullText.lastIndexOf(".", termIndex), fullText.lastIndexOf(";", termIndex));
            let sentenceEnd = fullText.indexOf(".", termIndex);
            if (sentenceEnd === -1) {
                sentenceEnd = fullText.length;
            }
            const sentenceEndSemicolon = fullText.indexOf(";", termIndex);
            if (sentenceEndSemicolon !== -1) {
                sentenceEnd = Math.min(sentenceEnd, sentenceEndSemicolon);
            }

            let result = null;
            if (sentenceBegin === -1) {
                result = fullText.substring(sentenceBegin, sentenceEnd).concat(" ...");
            } else {
                result = fullText.substring(sentenceBegin + ". ".length, sentenceEnd).concat(" ...");
            }

            return result.charAt(0).toUpperCase() + result.slice(1);
        }
    }

    return null;
}

function AnimalCard(props) {
    if (props.info === undefined) {
        return null;
    }

    var id = props.info.animal_id;
    if (id === undefined) {
        id = props.info.plant_id;
    }

    const searchList = props.searchList || [];

    let snippet = null;
    for (let i = 0; i < snippetCategories.length && snippet === null; i++) {
        snippet = getSnippet(props.info[snippetCategories[i]], props.searchList);
    }

    return (
        <div className="card paginationCard">
            <Link to={"/".concat(props.modelName).concat("/").concat(id)} id="card-link">
                <img className="card-img-top organismImage" src={props.info.image_link} alt={props.info.common_name}/>
                <div className="card-body">
                    <h5>
                        <Highlighter
                            highlightClassName="bg-warning p-0"
                            autoEscape={true}
                            searchWords={searchList}
                            textToHighlight={props.info.common_name}
                        />
                    </h5>
                    <h6>
                        <Highlighter
                            highlightClassName="bg-warning p-0"
                            autoEscape={true}
                            searchWords={searchList}
                            textToHighlight={props.info.sci_name}
                        />
                    </h6>
                    <h6>
                        <Highlighter
                            highlightClassName="bg-warning p-0"
                            autoEscape={true}
                            searchWords={searchList}
                            textToHighlight={props.info.category}
                        />
                    </h6>
                    <p className="animal-territories"> Lives in: {getTerritoryString(props.info.states)}</p>
                    <p>Habitats: {getHabitatString(props.info.habitats)}</p>
                    {
                        snippet &&
                        (
                            <p>
                                <Highlighter
                                    highlightClassName="bg-warning p-0"
                                    autoEscape={true}
                                    searchWords={searchList}
                                    textToHighlight={snippet}
                                />
                            </p>
                        )
                    }
                </div>
            </Link>
        </div>
    );
}

export default AnimalCard;