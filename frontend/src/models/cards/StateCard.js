import React from 'react';
import { Link } from 'react-router-dom';
import Highlighter from "react-highlight-words";

// We are going to display the image, name, human population, total land area,
// and percent of land area protected.

function getNumberWithCommas(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function convertToSquareMiles(acres) {
    const acresStr = acres.replace(/,/g, "");
    var miles = Math.floor(parseInt(acresStr)/640.0);
    return getNumberWithCommas(miles);
}

function StateCard(props) {
    if (props.info === undefined) {
        return null;
    }

    const searchList = props.searchList || [];

    return (
        <div className="paginationCard card">
                <Link to={"/states/".concat(props.info.code)} id="card-link">
                    <img className="card-img-top" src={props.info.map_image} alt={props.info.name}/>
                    <div className="card-body">
                        <h5>
                            <Highlighter
                                highlightClassName="bg-warning p-0"
                                autoEscape={true}
                                searchWords={searchList}
                                textToHighlight={props.info.name.concat(" (").concat(props.info.code).concat(")")}
                            />
                        </h5>
                        <h6>Human Population: {getNumberWithCommas(props.info.human_pop)}</h6>
                        <h6>Total Land Area: {convertToSquareMiles(props.info.land_area)} sq. miles</h6>
                        <h6>GAP 1 Area: {props.info.gap1} percent</h6>
                        <h6>GAP 2 Area: {props.info.gap2} percent</h6>
                        <h6>GAP 3 Area: {props.info.gap3} percent</h6>
                        <h6>Unprotected Area: {props.info.unprotected} percent</h6>
                    </div>
                </Link>
            </div>
    );
}

export default StateCard;