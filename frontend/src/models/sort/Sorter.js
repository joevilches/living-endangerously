import React, { Component } from 'react';
import {Form, Col, Button, ButtonToolbar} from 'react-bootstrap';


class Sorter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            field: null,
            direction: null,
            applied: false,
            fieldError: null,
            directionError: null
        };
        this.onDirectionChange = this.onDirectionChange.bind(this);
        this.onFieldChange = this.onFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.removeSort = this.removeSort.bind(this);
    }

    onDirectionChange(direction) {
        const onChange = (function() {
            this.setState({direction: direction});
        }).bind(this);
        return onChange;
    }

    onFieldChange(field) {
        const onChange = (function() {
            this.setState({field: field});
        }).bind(this);
        return onChange;
    }

    handleSubmit() {
        if (this.state.field === null || this.state.direction === null) {
            this.setState({
                fieldError: this.state.field === null ? "Please select a field." : null,
                directionError: this.state.direction === null ? "Please select a direction." : null,
            });
        } else {
            this.setState({fieldError: null, directionError: null, applied: true});
            this.props.apply(this.state.field, this.state.direction);
        }
    }

    removeSort() {
        this.props.remove();
        this.setState({field: null, direction: null, applied: false});
    }

    render() {
        const fieldItems = [];
        for (var i = 0; i < this.props.fields.length; i++) {
            const field = this.props.fields[i];
            fieldItems.push(
                <Form.Check key={i} type="radio" checked={field.key === this.state.field} onChange={this.onFieldChange(field.key)} label={field.name} />
            );
        }


        return (
            <div>
                <div className="row m-0">
                    <div className="col">
                        <h3>Sort Results</h3>
                    </div>
                </div>
                <div className="row m-0">
                    <div className="col">
                        <Form>
                            <Form.Row className="state-filter-row">
                                <Col>
                                    { fieldItems }
                                    <Form.Control className="d-none" isInvalid={this.state.fieldError !== null}/>
                                    <Form.Control.Feedback type="invalid">
                                        {this.state.fieldError}
                                    </Form.Control.Feedback>
                                </Col>
                                <Col>
                                    <Form.Check type="radio" checked={this.state.direction === "asc"} label="Ascending" onChange={this.onDirectionChange("asc")}/>
                                    <Form.Check type="radio" checked={this.state.direction === "desc"} label="Descending" onChange={this.onDirectionChange("desc")}/>
                                    <Form.Control className="d-none" isInvalid={this.state.directionError !== null}/>
                                    <Form.Control.Feedback type="invalid">
                                        {this.state.directionError}
                                    </Form.Control.Feedback>
                                </Col>
                            </Form.Row>
                            <Form.Row>
                                <Col>
                                    <ButtonToolbar>
                                        <Button variant="primary" onClick={this.handleSubmit}>Apply</Button>
                                        {
                                            this.state.applied && (
                                                <Button variant="danger" onClick={this.removeSort} className="m-l-10">Reset</Button>
                                            )
                                        }
                                    </ButtonToolbar>
                                </Col>
                            </Form.Row>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }

}

export default Sorter;
