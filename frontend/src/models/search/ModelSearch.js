import React, { Component } from 'react';
import {Form, Col, ButtonToolbar, Button} from 'react-bootstrap';

class ModelSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            query: "",
            applied: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.removeSearch = this.removeSearch.bind(this);
        this.getFilters = this.getFilters.bind(this);
    }

    handleChange(event) {
        this.setState({query: event.target.value});
    }

    handleKeyPress(event) {
        if (event.key === "Enter") {
            event.preventDefault();
            this.handleSubmit();
        }
    }

    getFilters(searchList) {
        const filters = [{
            "or": []
        }];
        for (const search in searchList) {
            for (const field of this.props.fields) {
                filters[0].or.push({
                    "name": field,
                    "op": "ilike",
                    "val": "%25".concat(searchList[search]).concat("%25")
                });
            }
        }
        return filters;
    }

    handleSubmit() {
        if (this.state.query === "") {
            this.removeSearch();
        } else {
            const searchList = this.state.query.split(" ");
            const filters = this.getFilters(searchList);
            this.props.setSearchList(searchList);
            this.props.apply(this.props.fields[0], filters);
            this.setState({applied: true});
        }
    }

    removeSearch() {
        this.props.setSearchList(null);
        this.props.remove(this.props.fields[0]);
        this.setState({query: "", applied: false});
    }

    render() {
        const modelName = this.props.modelName.charAt(0).toUpperCase() + this.props.modelName.slice(1)
        return (
            <div className="col">
                <h3>Search {modelName}</h3>
                <Form>
                    <Form.Row className="state-filter-row">
                        <Col>
                            <Form.Control
                                value={this.state.query}
                                placeholder={"Search"}
                                onChange={this.handleChange}
                                onKeyPress={this.handleKeyPress}
                            />
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <ButtonToolbar>
                                <Button variant="primary" onClick={this.handleSubmit}>Apply</Button>
                                {
                                    this.state.applied && (
                                        <Button variant="danger" onClick={this.removeSearch} className="m-l-10">Reset</Button>
                                    )
                                }
                            </ButtonToolbar>
                        </Col>
                    </Form.Row>
                </Form>
            </div>
        );
    }

}

export default ModelSearch;
