import React, { Component } from 'react';
import ModelSearch from './search/ModelSearch.js';
import Sorter from './sort/Sorter.js';
import './Model.css';
import Error from '../Error.js';

const OBJECTS_PER_PAGE = 9;

class Paginator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            objects: [],
            isLoaded: false,
            error: null,
            lastPage: -1,
            currentPage: 1,
            query: this.props.query,
            searchList: this.props.searchList
        };
        this.setSearchList = this.setSearchList.bind(this);
        this.applyFilters = this.applyFilters.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.applySort = this.applySort.bind(this);
        this.removeSort = this.removeSort.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    getObjects(currentPage, query) {
        const apiBaseURL = process.env.REACT_APP_API_BASE_URL || "https://api.livingendangerously.me";
        var url = apiBaseURL.concat("/api/").concat(this.props.modelName)
            .concat("?results_per_page=").concat(OBJECTS_PER_PAGE).concat("&page=").concat(currentPage);
        if (query != null) {
            url = url.concat("&q=").concat(query);
        }
        var cachedInfo = localStorage.getItem(url)
        if(cachedInfo !== null) {
            cachedInfo = JSON.parse(cachedInfo)
            this.setState({
                isLoaded: true,
                objects: cachedInfo.objects,
                lastPage: cachedInfo.total_pages,
                currentPage: currentPage,
                query: query
            });
            window.scrollTo(0, 0);
        }
        else {
            fetch(url)
            .then(function(res) {
                if (!res.ok) {
                    throw Error(res.statusText);
                }
                return res.json();
            }).then((result) => {
                    this.setState({
                        isLoaded: true,
                        objects: result.objects,
                        lastPage: result.total_pages,
                        currentPage: currentPage,
                        query: query
                    });
                    localStorage.setItem(url, JSON.stringify(result));
                    window.scrollTo(0, 0);
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error,
                        currentPage: currentPage
                    });
                }
            );
        }
    }

    componentDidMount() {
        var currentPage = parseInt(this.props.currentPage);
        if (currentPage < 1) {
            currentPage = 1;
        }
        this.getObjects(currentPage, this.props.query);
    }

    handlePageChange(event) {
        this.setState({isLoaded: false});
        var page;
        if (event.target.text === "Start") {
            page = 1;
        } else if (event.target.text === "Prev") {
            page = this.state.currentPage - 1;
        } else if (event.target.text === "Next") {
            page = this.state.currentPage + 1;
        } else if (event.target.text === "End") {
            page = this.state.lastPage;
        } else {
            page = parseInt(event.target.text);
        }
        this.getObjects(page, this.state.query);
    }

    getPaginationJSX(currentPage, lastPage) {
        const pagination = [];
        let keys = ["2", "3", "4"];
        let keyCount = 0;
        let curr = currentPage - 1;
        let prevClass = "page-item";
        let nextClass = "page-item";
        if (currentPage === lastPage || lastPage === 0) {
            curr = currentPage - 2;
            nextClass = "page-item disabled";
        }
        if (curr <= 0) {
            curr = 1;
            prevClass = "page-item disabled";
        }
        pagination.push(
            <li key="0" className="page-item">
                <a className="page-link" href="#" onClick={this.handlePageChange}>Start</a>
            </li>
        );
        pagination.push(
            <li key="1" className={prevClass}>
                <a className="page-link" href="#" onClick={this.handlePageChange}>Prev</a>
            </li>
        );
        while (keyCount < keys.length && curr <= lastPage) {
            let classnm = "page-item";
            if (curr === currentPage) {
                classnm = "page-item active";
            }

            pagination.push(
                <li key={keys[keyCount]} className={classnm}>
                    <a className="page-link" href="#" onClick={this.handlePageChange}>{curr}</a>
                </li>
            );
            keyCount++;
            curr++;
        }

        pagination.push(
            <li key="5" className={nextClass}>
                <a className="page-link" href="#" onClick={this.handlePageChange}>Next</a>
            </li>
        );
        pagination.push(
            <li key="6" className="page-item">
                <a className="page-link" href="#" onClick={this.handlePageChange}>End</a>
            </li>
        );
        return pagination;
    }

    setSearchList(searchList) {
        this.setState({searchList: searchList});
    }

    applySort(field, direction) {
        // The query might have filters/searching associated with it, so we
        // have to parse it into JSON, replace the order_by and stringify it again
        const order_by = [{
            "field": field,
            "direction": direction
        }];
        var query = this.state.query;
        if (query === undefined || query === null) {
            query = {
                "order_by": order_by
            };
        } else {
            query = JSON.parse(query);
            if (query.order_by !== undefined && query.order_by[0].field === field
                && query.order_by[0].direction === direction) {
                // The user has not changed anything, so just return
                return;
            }
            query.order_by = order_by;
        }
        this.setState({isLoaded: false});
        this.getObjects(1, JSON.stringify(query));
    }

    removeSort() {
        var query = this.state.query;
        if (query !== undefined && query !== null) {
            query = JSON.parse(query);
            delete query.order_by;
            this.getObjects(1, JSON.stringify(query));
        }
    }

    filterMatches(fieldName, filter) {
        return filter.name === fieldName || (filter.or !== undefined && filter.or[0].name === fieldName);
    }

    applyFilters(fieldName, filters) {
        var query = this.state.query;
        if (query === undefined || query === null) {
            query = {
                "filters": filters
            };
        } else {
            query = JSON.parse(query);
            if (query.filters === undefined || query.filters === null) {
                query.filters = filters;
            } else {
                // First remove all filters with the given key
                for (var i = 0; i < query.filters.length; i++) {
                    if (this.filterMatches(fieldName, query.filters[i])) {
                        query.filters.splice(i, 1);
                        i--;
                    }
                }

                // Add the new filters
                query.filters = query.filters.concat(filters);
            }
        }
        this.setState({isLoaded: false});
        this.getObjects(1, JSON.stringify(query));
    }

    removeFilter(fieldName) {
        let changed = false;
        var query = this.state.query;
        if (query !== undefined && query !== null) {
            query = JSON.parse(query);
            if (query.filters !== undefined && query.filters !== null) {
                for (var i = 0; i < query.filters.length; i++) {
                    if (this.filterMatches(fieldName, query.filters[i])) {
                        query.filters.splice(i, 1);
                        i--;
                        changed = true;
                    }
                }
            }
            if (changed) {
                this.setState({isLoaded: false});
                this.getObjects(1, JSON.stringify(query));
            }
        }
    }

    render() {

        if (!this.state.isLoaded) {
            return (
                <div id="loading-finished" className="container-fluid modelContainer">
                    <div className="row">
                        <div className="border-right col-lg-3 col-xs-12 col-sm-6">
                            {
                                this.props.searchFields &&
                                (
                                    <div>
                                        <ModelSearch
                                            fields={this.props.searchFields}
                                            modelName={this.props.modelName}
                                            apply={this.applyFilters}
                                            remove={this.removeFilter}
                                            setSearchList={this.setSearchList}
                                        />
                                        <hr />
                                    </div>
                                )
                            }
                            <Sorter fields={this.props.sortFields} apply={this.applySort} remove={this.removeSort} />
                            <hr />
                            { React.createElement(this.props.filterType, {apply: this.applyFilters, remove: this.removeFilter}, null) }
                        </div>
                        <div className="col-lg-9 col-xs-12 col-sm-6">
                            <div className="cards">
                                <div>
                                    <div className="lds-circle" style={{marginLeft: '30%'}}>
                                        <div/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        if (this.state.error != null) {
            return (<Error />);
        }

        // Set up the actual objects to display
        const rows = [];
        for (var i = 0; i < this.state.objects.length; i++) {
            rows.push(
                this.props.cardType({modelName: this.props.modelName, info: this.state.objects[i], searchList: this.state.searchList})
            );
        }

        // Display a message if there are no results
        if (this.state.lastPage === 0) {
            rows.push(<h4 key="0" id="result-none">No results found.</h4>);
        }

        // Set up the pagination box, but only if there is more than one page
        const currentPage = parseInt(this.state.currentPage);
        const pagination = (this.state.lastPage > 1) ? this.getPaginationJSX(currentPage, this.state.lastPage) : null;


        return (
            <div id="loading-finished" className="container-fluid modelContainer">

                <div className="row">
                    <div className="border-right col-lg-3 col-xs-12 col-sm-6">
                        {
                            this.props.searchFields &&
                            (
                                <div>
                                    <ModelSearch
                                        fields={this.props.searchFields}
                                        modelName={this.props.modelName}
                                        apply={this.applyFilters}
                                        remove={this.removeFilter}
                                        setSearchList={this.setSearchList}
                                    />
                                    <hr />
                                </div>
                            )
                        }
                        <Sorter fields={this.props.sortFields} apply={this.applySort} remove={this.removeSort} />
                        <hr />
                        { React.createElement(this.props.filterType, {apply: this.applyFilters, remove: this.removeFilter}, null) }
                    </div>
                    <div className="col-lg-9 col-xs-12 col-sm-6">
                        <div className="cards">
                            {rows}
                        </div>
                        <nav id="page-navigator" aria-label="Page navigation">
                            <ul className="pagination justify-content-center">
                                {pagination}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        );
    }
}

export default Paginator;
