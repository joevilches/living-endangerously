import React from 'react';
import './Data.css';

function Data(props) {
    return (
        <div>
            <h1 className='header'>Where we get our data and what we learned</h1>
            <p className='learned'>As a result of our data collection, we have found that there is not a direct correlation between the amount of vulnerable and endangered wildlife in a state, and the state's percentage of protected land. This leads us to assume that protected land is determined more by human needs and the politics of an individual state, versus the threats to flora and fauna.</p>
            <ul className='source-list'>
                    {
                        props.dataSources.map((source, index) => (
                            <li className='source'>
                                <div key={index}>
                                    <img className='source-img' src={source.img_path} alt={source.name}></img>
                                    <a href={source.url}>{source.name}</a>
                                    <p className='source-desc'>{source.description}</p>
                                    <br />
                                </div>
                            </li>
                        ))
                    }
            </ul>


        </div>
    );
}

export default Data;