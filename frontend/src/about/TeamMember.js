import React from 'react';
import './About.css';

function TeamMember(props) {
    return (
        <div className="col-sm-6 aboutColumn">
            <div className="card about">
                <img className="card-img-top" src={props.info.image} alt={props.info.name} />
                <div className="card-body">
                    <h5 className="card-title">{props.info.name}</h5>
                    <h6 className="card-subtitle mb-3 text-muted">{props.info.bio}<a href={props.info.spirit_animal_link}>{props.info.spirit_animal}</a></h6>
                    <p><b>Responsibilities:</b> {props.info.responsibilities}<br/>
                        <b>Commits:</b> {props.info.commits}<br/>
                        <b>Issues:</b> {props.info.issues}<br/>
                        <b>Unit Tests:</b> {props.info.unitTests}</p>
                </div>
            </div>
        </div>
    );
}

export default TeamMember;