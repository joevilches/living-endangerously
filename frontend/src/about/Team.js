import React from 'react';
import TeamMember from './TeamMember';
import './Team.css';

function Team(props) {
    return (
        <div>
            <h1 className="header">Group Members</h1>
            <div className="row aboutTeamRow">
                {
                    Object.keys(props.users).map((key, index) => (
                        <TeamMember key={index} info={props.users[key]} />
                    ))
                }
            </div>
        </div>
    );
}

export default Team;