import React, { Component } from 'react';
import update from 'immutability-helper';
import './About.css';
import Error from '../Error.js';
import MainNavbar from '../navbar/MainNavbar';
import Tools from './Tools'
import Data from './Data'
import Stats from './Stats'
import Team from './Team'
import Links from './Links'

class About extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tab: "team",
            error: null,
            isLoaded: false,
            totalCommits: 0,
            totalIssues: 0,
            totalUnitTests: 142,
            dataSources: [
                {
                    name: 'United States Geological Survey',
                    url: 'https://gapanalysis.usgs.gov/padus/',
                    description: 'We used this RESTful API to get conservation information on states. Data such as percent of land protected and GAP status codes.',
                    img_path: '../../usgs.png'
                },
                {
                    name: 'NatureServe',
                    url: 'https://services.natureserve.org/TechnicalResources/indexofservices.jsp',
                    description: 'We used this RESTful API to get information on both plants and animals, including scientific name, conservation status and habitat. This REST API returned XML, which we parsed using an XML parser.',
                    img_path: '../../natureserve.png'
                },
                {
                    name: 'Encyclopedia of Life',
                    url: 'https://eol.org/docs/what-is-eol/data-services ',
                    description: 'We also used this REST API to get biological information on animals. Data related to descriptions of the animals and their lifestyles.',
                    img_path: '../../eol.png'
                },
                {
                    name: 'United States Census Bureau',
                    url: 'https://www.census.gov/data/developers/data-sets/popest-popproj/popest.html',
                    description: 'We used this RESTful API to get demogrpahic imformation on states. Data such as population, names of the states, and state codes.',
                    img_path: '../../census.png'
                },
            ],
            tools: [
                {
                    name: 'AWS',
                    url: 'https://aws.amazon.com/',
                    use: 'We use AWS to host our website, web server, and database.',
                    about: 'Amazon Web Services can be used in many different ways. One of the more common services they provide is the ability to host web servers and sites. They also have services for NoSQL databases, cloud computing, and many others.',
                    img_path: '../../aws.png',
                },
                {
                    name: 'React.js',
                    url: 'https://reactjs.org/',
                    use: 'We use React to create the frontend of our website. Every button, search bar, and page you interact with is developed with React.',
                    about: 'React is a javascript library to quickly create user interfaces. It handles the rendering of CSS, HTML, and Javascript on websites and organizes them in a convenient way.',
                    img_path: '../../react.png'
                },
                {
                    name: 'Postman',
                    url: 'https://www.getpostman.com/',
                    use: 'We use Postman to both build and test our own API and to experiment with NatureServe\'s API, our main data source.',
                    about: 'Postman is a tool that simplifies API development. They have features to test, build, and experiment with different APIs.',
                    img_path: '../../postman.png'
                },
                {
                    name: 'Bootstrap',
                    url: 'https://getbootstrap.com/',
                    use: 'We use bootstrap to quickly style certain elements of our website. Things such as buttons, navbars, and input boxes were all styled with Boostrap.',
                    about: 'Bootstrap is a CSS Framework. This means that much of the design you see on our website were taken from their extensive library of styles.',
                    img_path: '../../bootstrap.png'
                },
                {
                    name: 'Namecheap',
                    url: 'https://www.namecheap.com/',
                    use: 'Our domain, livingendangerously.me, was obtained through namecheap.',
                    about: 'Namecheap provides hostnames.',
                    img_path: '../../namecheap.png'
                },
                {
                    name: 'Flask',
                    url: 'http://flask.pocoo.org/',
                    use: 'We use Flask to create our API. Our website interacts with the API we created with Flask which then interacts with our database.',
                    about: 'Flask is a webframework built in Python. Flask aids in the development of web sites, especially in the backend with APIs and various other services.',
                    img_path: '../../flask.png'
                },
                {
                    name: 'D3',
                    url: 'https://d3js.org/',
                    use: 'We use D3 to construct complicated visualizations across the website. Most of the D3 we use can be seen under the "Big Picture" tab.',
                    about: 'D3 is a javascript library for creating visually engaging maps, graphs, and charts. D3 can quickly create a diverse set of visualization for any dataset you give it.',
                    img_path: '../../d3.png'
                },
                {
                    name: 'PostgreSQL',
                    url: 'https://www.postgresql.org/',
                    use: 'We use PostgreSQL as our main Data store. Every bit of information in the model and instance pages are stored in our PostgreSQL database. Our DB is hosted in AWS.',
                    about: 'PostgreSQL is a relational database designed for the permanent storage of information.',
                    img_path: '../../postgres.png'
                },
                {
                    name: 'GitLab',
                    url: 'https://gitlab.com/',
                    use: 'We use GitLab as our Git repo. We also make use of their CI pipeline to make sure our code passes all of its tests.',
                    about: 'GitLab, similar to GitHub, is an online Git repository and DevOps lifecycle tool. It stores and updates code for various projects and provids tools like CI/CD pipelines to make software development more organized.',
                    img_path: '../../gitlab.png'
                },
                {
                    name: 'Docker',
                    url: 'https://www.docker.com/',
                    use: 'We use Docker in order to make sure that the versions and dependencies we use are uniform so our software has the same outcome no matter the machine.',
                    about: 'Docker eases the development process by packaging different libraries, versions, and softwares together in one container that any dev can quickly make use of.',
                    img_path: '../../docker.png'
                },
                {
                    name: 'Selenium',
                    url: 'https://www.seleniumhq.org/',
                    use: 'We use Selenium to both test our UI and to pull our pictures for each plant and animal.',
                    about: 'Selenium is for testing the user interface of a website. It can, for example, make sure certain buttons link to where they need to go and HTML elements have the correct tags.',
                    img_path: '../../selenium.png'
                },
                {
                    name: 'Mocha',
                    url: 'https://mochajs.org/',
                    use: 'We use Mocha to test our frontend javascript code.',
                    about: 'Mocha is a javascript unit testing framework to make sure that code does what it is expected to do.',
                    img_path: '../../mocha.png'
                },
                {
                    name: 'MaterialUI',
                    url: 'https://material-ui.com/',
                    use: 'We use MaterialUI in the same way we use Bootstrap. In fact, the tooltip you are reading right now is made with MaterialUI!',
                    about: 'Similar to Bootstrap, MaterialUI is a CSS framework designed to make use of predefined CSS styles.',
                    img_path: '../../mui.png'
                }
            ],
            users: {
                'joevilches@gmail.com': {
                    name: 'Joseph Vilches',
                    image: '../../josephvilches.jpg',
                    bio: 'My spirit animal is the ',
                    spirit_animal: 'Bog Turtle.',
                    responsibilities: 'Database Engineer, Website Design, Frontend Engineer',
                    gitlab_id: '3440511',
                    issues: 0,
                    commits: 0,
                    unitTests: 10,
                    spirit_animal_link: 'https://www.livingendangerously.me/animals/748'
                },
                'mkarora@utexas.edu': {
                    name: 'Maya Kothare-Arora',
                    image: '../../mayakothare-arora.jpg',
                    bio: 'My spirit animal is the ',
                    spirit_animal: 'Yellow-bellied Marmot.',
                    responsibilities: 'Frontend Engineer, Testing, AWS Guru',
                    gitlab_id: '3501148',
                    issues: 0,
                    commits: 0,
                    unitTests: 34,
                    spirit_animal_link: 'https://www.livingendangerously.me/animals/27'
                },
                'klbowen122@gmail.com': {
                    name: 'Kendall Bowen',
                    image: '../../kendallbowen.jpg',
                    bio: 'My spirit animal is the ',
                    spirit_animal: 'Opossum.',
                    responsibilities: 'Frontend Engineer, Database Population, API Collection/Research',
                    gitlab_id: '3494417',
                    issues: 0,
                    commits: 0,
                    unitTests: 21,
                    spirit_animal_link: 'https://www.livingendangerously.me/animals/1'
                },
                'twatson1698@gmail.com': {
                    name: 'Terrell Watson',
                    image: '../../terrellwatson.jpg',
                    bio: 'My spirit animal is the ',
                    spirit_animal: 'Red Wolf.',
                    responsibilities: 'Frontend Engineer, Testing, API Collection/Research',
                    gitlab_id: '3440508',
                    issues: 0,
                    commits: 0,
                    unitTests: 41,
                    spirit_animal_link: 'https://www.livingendangerously.me/animals/364'
                },
                'jackstenglein@utexas.edu': {
                    name: 'Jack Stenglein',
                    image: '../../jackstenglein.jpg',
                    bio: 'My spirit animal is ',
                    spirit_animal: 'Joe Vilches.',
                    responsibilities: 'Backend Engineer, Frontend Engineer, Testing',
                    gitlab_id: '3486390',
                    issues: 0,
                    commits: 0,
                    unitTests: 36,
                    spirit_animal_link: 'https://www.livingendangerously.me/animals/514'
                }
            }
        };
    }

    componentDidMount() {
        // Fetch individual issues
        for (let key in this.state.users) {
            let url = "https://gitlab.com/api/v4/projects/10945064/issues?per_page=100&assignee_id=".concat(this.state.users[key].gitlab_id)
                        .concat("&private_token=gVSso5BJs_GgmcATvnw8");
            fetch(url)
                .then(function(res) {
                    if (!res.ok) {
                        throw Error(res.statusText);
                    }
                    return res.json();
                }).then((result) => {
                    this.setState({
                        isLoaded: true,
                        users: update(this.state.users, {[key]: {issues: {$set: result.length}}}),
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error
                    });
                }
            )
        }

        this.fetchCommits();
        this.fetchIssues();
    }

    // Fetch individual and total commits
    async fetchCommits() {
        var page = 0;
        var finished = false;
        while(!finished) {
            // Fetch each page of commits
            ++page;
            await fetch("https://gitlab.com/api/v4/projects/10945064/repository/commits?all=true&per_page=100&private_token=gVSso5BJs_GgmcATvnw8&page=".concat(page))
                .then(function(res) {
                    if (!res.ok) {
                        throw Error(res.statusText);
                    }
                    return res.json();
                }).then((commits) => {
                    this.setState({
                        isLoaded: true,
                        totalCommits: this.state.totalCommits + commits.length
                    });

                    // Update individual commits
                    commits.forEach(function(commit) {
                        var committer_email = commit.committer_email.toLowerCase();
                        this.setState({
                            users: update(this.state.users, {[committer_email]: {commits: {$apply: function(x) {return x + 1;}}}})
                        });
                    }.bind(this));

                    // Break out of loop if the GitLab API didn't return a full page
                    if (commits.length < 100) {
                        finished = true;
                    }
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error
                    });
                    finished = true;
                }
            );
        }
    }

    async fetchIssues() {
        var page = 0;
        var finished = false;
        while(!finished) {
            // Fetch each page of commits
            ++page;
            await fetch("https://gitlab.com/api/v4/projects/10945064/issues?per_page=100&private_token=gVSso5BJs_GgmcATvnw8&page=".concat(page))
                .then(function(res) {
                    if (!res.ok) {
                        throw Error(res.statusText);
                    }
                    return res.json();
                }).then((issues) => {
                    this.setState({
                        isLoaded: true,
                        totalIssues: this.state.totalIssues + issues.length
                    });

                    // Break out of loop if the GitLab API didn't return a full page
                    if (issues.length < 100) {
                        finished = true;
                    }
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error
                    });
                    finished = true;
                }
            )
        }
    }

    render() {
        const { error, isLoaded, totalCommits, totalIssues, totalUnitTests, dataSources, tools, users } = this.state;
        if (error) {
            return <Error />;
        } else if (!isLoaded) {
            return (
                <div>
                    <MainNavbar from="about-page"/>
                    <div className="lds-circle" style={{marginLeft: '40%'}}>
                        <div></div>
                    </div>
                </div>);
        } else {
            return (
                <div id="gitlab-stats-loaded">
                    <MainNavbar from="about-page"/>
                    <div className="aboutHeader">
                        <img src="../../bird-formation.jpg" className="aboutHeaderPicture" alt="Birds flying in formation"/>
                        <h1 className="about-us">About Us</h1>
                        <span className="about-us-sub">Who we are. What we do. How we do it.</span>

                        <h1 className="motto">Every plant and every animal has a story</h1>
                        <p className="about-statement">The goal of our project is to encourage civic interest and engagement in habitat preservation by providing users with information about endangered plants and animals, as well as the states those species live in. This is meant to increase awareness of preservation issues, as well as make clear the points of entry for a user to begin or continue contributing to preservation efforts. Our site's intended users are anyone who wants to learn more about what they can do to combat species loss in their state.</p>
                        <hr></hr>

                        <ul id="selector">
                            <li id={this.state.tab === "team" ? "active" : null} className="selector-elem" onClick={() => this.selectTab("team")}>Meet the team</li>
                            <li id={this.state.tab === "data" ? "active" : null} className="selector-elem" onClick={() => this.selectTab("data")}>Our data</li>
                            <li id={this.state.tab === "tools" ? "active" : null} className="selector-elem" onClick={() => this.selectTab("tools")}>Our tools</li>
                            <li id={this.state.tab === "stats" ? "active" : null} className="selector-elem" onClick={() => this.selectTab("stats")}>GitLab stats</li>
                            <li id={this.state.tab === "links" ? "active" : null} className="selector-elem" onClick={() => this.selectTab("links")}>Other links</li>
                        </ul>

                        <div className="tab-wrapper">
                            {this.state.tab === "team" ? <Team users={users} /> : null}
                            {this.state.tab === "data" ? <Data dataSources={dataSources}/> : null}
                            {this.state.tab === "tools" ? <Tools tools={tools}/> : null}
                            {this.state.tab === "stats" ? <Stats totalCommits={totalCommits} totalIssues={totalIssues} totalUnitTests={totalUnitTests}/> : null}
                            {this.state.tab === "links" ? <Links/> : null}
                        </div>
                    </div>
                </div>
            );
        }
    }

    selectTab(val) {
        this.setState({tab : val});
    }
}

export default About;