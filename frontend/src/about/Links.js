import React from 'react';
import './Links.css';

function Links(props) {
    return (
        <div>
            <h1 className='header'>Feel free to look at our repo or use our API</h1>
            <div className='link-con-git'><a href="https://gitlab.com/joevilches/living-endangerously"><img className='link-img' src='../../repo.png' alt='Repository Link'></img></a></div>
            <div className='link-con-api'><a href="https://documenter.getpostman.com/view/1135214/S11KQK7N"><img className='link-img' src='../../api.jpeg' alt='API Link'></img></a></div>
            <br/>
        </div>
    );
}

export default Links;