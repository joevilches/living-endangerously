import React from 'react';
import './Stats.css';

function Stats(props) {
    return (
        <div>
            <h1 className='header' >GitLab Repository Stats</h1>
            <ul className='stats-list'>
                <li className='stat'>
                    <span className='number'>{props.totalCommits}</span>
                    <p className='stat-name'>Commits</p>
                </li>
                <li className='stat'>
                    <span className='number'>{props.totalIssues}</span>
                    <p className='stat-name'>Issues</p>
                </li>
                <li className='stat'>
                    <span className='number'>{props.totalUnitTests}</span>
                    <p className='stat-name'>Unit Tests</p>
                </li>
            </ul>
        </div>
    );
}

export default Stats;