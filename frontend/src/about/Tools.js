import React, { Component } from 'react';
import './Tools.css';
import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Zoom from '@material-ui/core/Zoom';
import {withStyles} from '@material-ui/core/styles';

function Tools(props) {
    return (
        <div>
            <h1 className='tool-header'>Click on a tool to learn more about how we use it</h1>
            <ul className='toolbelt'>
                {
                    props.tools.slice(0,4).map((tool, index) => (
                        <li class='tool' key={index}>
                            <StyledToolPopper tool={tool}></StyledToolPopper>
                        </li>
                    ))
                }
            </ul>
            <ul className='toolbelt'>
                {
                    props.tools.slice(4,9).map((tool, index) => (
                        <li class='tool' key={index}>
                            <StyledToolPopper tool={tool}></StyledToolPopper>
                        </li>
                    ))
                }
            </ul>
            <ul className='toolbelt'>
                {
                    props.tools.slice(9,13).map((tool, index) => (
                        <li class='tool' key={index}>
                            <StyledToolPopper tool={tool}></StyledToolPopper>
                        </li>
                    ))
                }
            </ul>
        </div>
    );
}

const styles = {
    tooltip: {
        backgroundColor: "white",
        color: 'black',
        width: '500px',
        height: '300px',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
    },
    popper: {
        opacity: 1.0
    }
  };

class ToolPopper extends Component {

    state = {
        open: false,
    };

    handleTooltipClose = () => {
        this.setState({ open: false });
    };

    handleTooltipOpen = () => {
        this.setState({ open: !this.state.open });
    };

    render() {
        return (
            <ClickAwayListener onClickAway={this.handleTooltipClose}>
                <Tooltip
                PopperProps={{
                    disablePortal: true,
                }}
                onClose={this.handleTooltipClose}
                open={this.state.open}
                disableFocusListener
                disableHoverListener
                disableTouchListener
                interactive
                TransitionComponent={Zoom}
                classes={this.props.classes}
                title={
                    <React.Fragment>
                        <div class='tool-containter'>
                            <a class='tool-name' href={this.props.tool.url}>{this.props.tool.name}</a>
                            <p class='tool-desc'><span class='tool-desc-header'>About the tool: </span>{this.props.tool.about}</p>
                            <p class='tool-desc'><span class='tool-desc-header'>How we use it: </span>{this.props.tool.use}</p>
                        </div>
                    </React.Fragment>
                }
                >
                    <span>
                        <img src={this.props.tool.img_path}
                            alt={this.props.tool.name}
                            onClick={this.handleTooltipOpen}
                            class='tool-img'>
                        </img>
                    </span>
                </Tooltip>
            </ClickAwayListener>
        )
    }
}

const StyledToolPopper = withStyles(styles)(ToolPopper);
export default Tools;