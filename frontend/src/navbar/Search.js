import React, { Component } from 'react'
import {Form, FormControl, Button} from 'react-bootstrap'
import { withRouter } from 'react-router-dom'

import './Search.css';


class Search extends Component {

    constructor(props) {
        super(props);
        this.state = {
            query: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }



    handleChange(event) {
        this.setState({query: event.target.value});
    }

    handleKeyPress(event) {
        if (event.key === "Enter") {
            event.preventDefault();
            this.searchButton.click();
        }
    }

    render() {
        return (
            <Form inline className={this.props.className}>
                <FormControl type="text" placeholder={"Search Site"} className="mr-sm-2" onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
                <Button type="button" ref={button => this.searchButton = button} className="search-button" variant="outline-success" href={"/results?search=".concat(this.state.query)}>
                    Search
                </Button>
            </Form>
      );
    }
}
export default withRouter(Search);
