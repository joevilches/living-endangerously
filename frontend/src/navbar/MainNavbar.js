import React, { Component } from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import Search from './Search.js'
import './MainNavbar.css'

class MainNavbar extends Component {
    render() {
      return (
        <Navbar expand="lg" id="navbar">
            <Navbar.Brand href="/">
                <div id="navbar-title">
                <img src="../../turtle.png" alt='turtle' id="navbar-turtle"/>
                LIVING ENDANGEROUSLY
                </div>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/animals">
                        <div id="navbar-link">
                            <span className={getClassName(this.props.from, "animal-page")} id="marg">ANIMALS</span>
                        </div>
                    </Nav.Link>
                    <Nav.Link href="/plants">
                        <div id="navbar-link">
                            <span className={getClassName(this.props.from, "plant-page")} id="marg">PLANTS</span>
                        </div>
                    </Nav.Link>
                    <Nav.Link href="/states" >
                        <div id="navbar-link">
                            <span className={getClassName(this.props.from, "state-page")} id="marg">STATES</span>
                        </div>
                    </Nav.Link>
                    <Nav.Link href="/about">
                        <div id="navbar-link">
                            <span className={getClassName(this.props.from, "about-page")} id="marg">ABOUT</span>
                        </div>
                    </Nav.Link>
                    <Nav.Link href="/visualization">
                        <div id="navbar-link">
                            <span className={getClassName(this.props.from, "visual-page")} id="marg">BIG PICTURE</span>
                        </div>
                    </Nav.Link>
                </Nav>
                <Search/>
            </Navbar.Collapse>
        </Navbar>
      );
    }
  }

function getClassName(from, linkName) {
    if(linkName === from) {
        return "active";
    }
    return "";
}


export default MainNavbar;
