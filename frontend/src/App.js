import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Search from './navbar/Search';
import './App.css';

class App extends Component {
  render() {
    return (
      <div id="front-page">
        <img src="../../goat.jpeg" alt="goat" id="goat"/>
        <div id="nav">
          <h2 id="title">
            <Link to="/" className="link" id="title-link">
              <img src="../turtle.png" alt="turtle" id="title-turtle"/>
              <span id="living">LIVING ENDANGEROUSLY</span>
            </Link>
          </h2>
          <div id="models">
            <Link to="/animals" className="link" id="animal-link">ANIMALS</Link>
            <Link to="/plants" className="link" id="plant-link">PLANTS</Link>
            <Link to="/states" className="link" id="state-link">STATES</Link>
            <Link to="/about" className="link" id="about-link">ABOUT</Link>
              <Link to="/visualization" className="link" id="visual-link">BIG PICTURE</Link>
          </div>
          <Search className="search-input" />
          <div id="about">
          </div>
        </div>
        <h1 id="statement">
          Thousands of species <br /> face extinction every day
        </h1>
        <Link to="/animals" id="link">
          <button id="button" type="button" className="btn btn-lg">
            Find out who needs help
          </button>
        </Link>
        <div id="quote-page">
          <div id="quote-text">
            <h3 id="section-name">What we do</h3>
            <p id="actual-quote" className="quotation"><span id="sans-font">The greatness of a nation can be judged by the way its animals are treated.</span></p>
            <p id="quote-attrib" className="footer">- Mahatma Gandhi</p>
            <p id="mission-statement">Living Endangerously aims to enlighten the general public about the
                                      conservation status of American plants and animals. We believe that by
                                      doing so these animals will have a better chance of overcoming extinction. Just like Gandhi, we believe
                                      that our governments are only as good as the way they treat animals. As a result, we
                                      also detail how different states deal with conservation. We hope you enjoy
                                      exploring!</p>
            <Link to="/about" id="link">
              <button id="find-button" className="btn btn-lg">Find out more</button>
            </Link>
          </div>
          <div id="quote-picture">
            <img src="../../jack_picture.jpg" alt="waterfall" id="waterfall"></img>
          </div>
        </div>
        <div id="model-buttons">
          <span id="section-name">What we want to show you</span>
          <p id="model-subtitle">We have conservation data on thousands of species and encourage you to explore them all.</p>
          <ul id="model-list">
            <li id="model-element">
              <Link to="/animals" id="model-link" className="animal">
                <img className="animal" src="../fox.svg" alt="animals"/>
                <p>Animals</p>
              </Link>
            </li>
            <li id="model-element">
              <Link to="/plants" id="model-link" className="leaf">
                <img className="leaf" src="../leaf-solid.svg" alt="plants"/>
                <p>Plants</p>
              </Link>
            </li>
            <li id="model-element">
              <Link to="/states" id="model-link" className="flag">
                <img className="flag" src="../flag-solid.svg" alt="states"/>
                <p>States</p>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default App;
