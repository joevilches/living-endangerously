import React, { Component } from 'react';
import './StateInstance.css'
import OrganismCarousel from '../carousel/OrganismCarousel.js';
import Error from '../Error.js';
import MainNavbar from '../navbar/MainNavbar';
import StateChartRow from './StateChartRow.js';


class StateInstance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            error: null
        }
    }

    getNumberWithCommas(num) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    componentDidMount() {
        var apiBaseURL = process.env.REACT_APP_API_BASE_URL || "https://api.livingendangerously.me";
        const url = apiBaseURL.concat("/api/states/").concat(this.props.match.params.id);

        fetch(url)
        .then(function(res) {
            if (!res.ok) {
                throw Error(res.statusText);
            }
            return res.json();
        }).then((result) => {
                this.setState({
                    isLoaded: true,
                    territory: result
                });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                this.setState({
                    isLoaded: true,
                    error: error
                });
            });
    }

    render() {

        if (!this.state.isLoaded) {
            return null;
        }

        if (this.state.error) {
            return <Error />;
        }

        const state = this.state.territory;
        const acresStr = state.land_area.replace(/,/g, "");
        var miles = Math.floor(parseInt(acresStr)/640);
        return (
            <div>
                <MainNavbar from="state-page"/>
                <div id="loading-finished" className="container-fluid">
                    <div className="centered">
                        <h1 id="territory-name">{state.name}</h1>
                        <img className="principalImage" src={state.map_image} alt={state.name} />
                        <h3 className="modelTableHeader">Key Statistics</h3>
                        <table className="table table-bordered stateTable">
                            <tbody>
                                <tr>
                                    <th scope="row">Human Population</th>
                                    <td>{this.getNumberWithCommas(state.human_pop)}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Total Land Area</th>
                                    <td>{this.getNumberWithCommas(miles)} sq. miles</td>
                                </tr>
                                <tr>
                                    <th scope="row">Population Density</th>
                                    <td>{Math.round( (state.human_pop/miles) * 10 ) / 10} people per sq. mile</td>
                                </tr>
                            </tbody>
                        </table>

                        <StateChartRow state={state} />

                    </div>

                    <h3 className="modelTableHeader">Animals in {state.name}</h3>
                    <OrganismCarousel id="animal-carousel" linkBase="/animals/" organisms={state.animals} />

                    <br />

                    <h3 className="modelTableHeader">Plants in {state.name}</h3>
                    <OrganismCarousel id="plant-carousel" linkBase="/plants/" organisms={state.plants} />

                    <br />
                    <div className="centered">
                        <h3 id="gapCodeList" className="modelTableHeader">GAP Status Codes</h3>
                        <table className="table table-bordered w-75">
                            <thead>
                                <tr>
                                  <th scope="col">GAP Code</th>
                                  <th scope="col">Meaning</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">GAP 1 Protected Area</th>
                                    <td>An area having permanent protection from conversion of natural land cover and a mandated management plan in operation to maintain a natural state within which disturbance events (of natural type, frequency, intensity, and legacy) are allowed to proceed without interference or are mimicked through management.</td>
                                </tr>
                                <tr>
                                    <th scope="row">GAP 2 Protected Area</th>
                                    <td>An area having permanent protection from conversion of natural land cover and a mandated management plan in operation to maintain a primarily natural state, but which may receive uses or management practices that degrade the quality of existing natural communities, including suppression of natural disturbance.</td>
                                </tr>
                                <tr>
                                    <th scope="row">GAP 3 Protected Area</th>
                                    <td>An area having permanent protection from conversion of natural land cover for the majority of the area, but subject to extractive uses of either a broad, low intensity type (for example, logging, Off-Highway Vehicle recreation) or localized intense type (for example, mining). It also confers protection to federally listed endangered and threatened species throughout the area.</td>
                                </tr>
                                <tr>
                                    <th scope="row">Unprotected Area</th>
                                    <td>There are no known public or private institutional mandates or legally recognized easements or deed restrictions held by the managing entity to prevent conversion of natural habitat types to anthropogenic habitat types. The area generally allows conversion to unnatural land cover throughout or management intent is unknown.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default StateInstance;
export const getNumberWithCommas = StateInstance.prototype.getNumberWithCommas;
