import React from 'react';
import {
    ResponsiveContainer, PieChart, Pie, Legend, Cell,
} from 'recharts';

const COLORS = ['#5fcc9c', '#09194f', '#215b63', '#c70039', '#f46942', '#99a021', '#a615ab' ];

const ORGANISM_CATEGORY_COLORS = {
    'Secure': '#5fcc9c',
    'Apparently Secure': '#699380',
    'Vulnerable': '#e99d30',
    'Imperiled': '#f0ee41',
    'Critically Imperiled': '#99a021',
    'Extinct in the Wild': '#c70039',
    'Possibly Extinct': '#f46942',
    'Presumed Extinct': '#a615ab',
    'Not Ranked': '#09194f',
    'Not Applicable': '#000000'
};


function getGapStatistics(state) {
    var gapData = [
        { name: 'GAP 1', value: state.gap1 },
        { name: 'GAP 2', value: state.gap2 },
        { name: 'GAP 3', value: state.gap3 },
        { name: 'Unprotected', value: state.unprotected },
    ];
    gapData.forEach(function(element) {
        var value = element.value;
        if(value.includes("< 1")) {
            value = .5;
            element.value = value;
        } else {
            element.value = parseInt(value);
        }
    });

    return gapData;
}

function removeZeroValues(statistics) {
    for (let i = 0; i < statistics.length; ++i) {
        if (statistics[i].value === 0) {
            statistics.splice(i, 1);
            --i;
        }
    }
}

function getAnimalStatistics(state) {
    const animalStatistics = [
        {name: 'Secure', value: state.secure_animals},
        {name: 'Apparently Secure', value: state.apparently_secure_animals},
        {name: 'Vulnerable', value: state.vulnerable_animals},
        {name: 'Imperiled', value: state.imperiled_animals},
        {name: 'Critically Imperiled', value: state.critically_imperiled_animals},
        {name: 'Extinct in the Wild', value: state.extinct_wild_animals},
        {name: 'Possibly Extinct', value: state.possibly_extinct_animals},
        {name: 'Presumed Extinct', value: state.presumed_extinct_animals},
        {name: 'Unrankable', value: state.unrankable_animals},
        {name: 'Not Ranked', value: state.not_ranked_animals},
        {name: 'Not Applicable', value: state.not_applicable_animals}
    ];
    removeZeroValues(animalStatistics);
    return animalStatistics;
}

function getPlantStatistics(state) {
    const plantStatistics = [
        {name: 'Secure', value: state.secure_plants},
        {name: 'Apparently Secure', value: state.apparently_secure_plants},
        {name: 'Vulnerable', value: state.vulnerable_plants},
        {name: 'Imperiled', value: state.imperiled_plants},
        {name: 'Critically Imperiled', value: state.critically_imperiled_plants},
        {name: 'Extinct in the Wild', value: state.extinct_wild_plants},
        {name: 'Possibly Extinct', value: state.possibly_extinct_plants},
        {name: 'Presumed Extinct', value: state.presumed_extinct_plants},
        {name: 'Unrankable', value: state.unrankable_plants},
        {name: 'Not Ranked', value: state.not_ranked_plants},
        {name: 'Not Applicable', value: state.not_applicable_plants}
    ];
    removeZeroValues(plantStatistics);
    return plantStatistics;
}

function StateChartRow(props) {
    if (props.state === undefined) {
        return null;
    }

    const animalStatistics = getAnimalStatistics(props.state);
    const plantStatistics = getPlantStatistics(props.state);
    const gapStatistics = getGapStatistics(props.state);

    return (
        <div className="row state-chart-row">
            <div className="col w-33">
                <h3 className="text-center">Animal Breakdown</h3>
                <ResponsiveContainer width="100%" height={430}>
                    <PieChart>
                        <Pie
                            data={animalStatistics}
                            outerRadius={150}
                            fill="#8884d8"
                            paddingAngle={3}
                            minAngle={2}
                            dataKey="value"
                            label
                        >
                            {
                                animalStatistics.map(function(entry, index) {
                                    return (<Cell key={`cell-${index}`} fill={ORGANISM_CATEGORY_COLORS[entry.name]} />);
                                })
                            }
                        </Pie>
                        <Legend verticalAlign="bottom"/>
                    </PieChart>
                </ResponsiveContainer>
            </div>

            <div className="col w-33">
                <h3 className="text-center">Plants Breakdown</h3>
                <ResponsiveContainer width="100%" height={430}>
                    <PieChart>
                        <Pie
                            data={plantStatistics}
                            outerRadius={150}
                            fill="#8884d8"
                            paddingAngle={3}
                            minAngle={2}
                            dataKey="value"
                            label
                        >
                            {
                                plantStatistics.map(function(entry, index) {
                                    return (<Cell key={`cell-${index}`} fill={ORGANISM_CATEGORY_COLORS[entry.name]} />);
                                })
                            }
                        </Pie>
                        <Legend verticalAlign="bottom"/>
                    </PieChart>
                </ResponsiveContainer>
            </div>

            <div className="col w-33">
                <h3 className="text-center">GAP Protected Area Percentages <a href="#gapCodeList" className="badge badge-info">?</a></h3>
                <ResponsiveContainer width="100%" height={430}>
                    <PieChart>
                        <Pie
                            data={gapStatistics}
                            innerRadius={100}
                            outerRadius={150}
                            fill="#8884d8"
                            paddingAngle={3}
                            dataKey="value"
                            label
                        >
                            {
                                gapStatistics.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                            }
                        </Pie>
                        <Legend verticalAlign="bottom"/>
                    </PieChart>
                </ResponsiveContainer>
            </div>
        </div>
    );
}

export default StateChartRow;
