import React, { Component } from 'react';
import Classification from './Classification.js';
import OrganismCarousel from '../carousel/OrganismCarousel.js';
import USAMap from '../usa-map/USAMap.js';
import Error from '../Error.js';
import MainNavbar from '../navbar/MainNavbar';
import './OrganismInstance.css';

class PlantInstance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            plant: null
        };
    }

    componentDidMount() {
        var apiBaseURL = process.env.REACT_APP_API_BASE_URL || "https://api.livingendangerously.me";
        const url = apiBaseURL.concat("/api/plants/").concat(this.props.match.params.id);

        fetch(url)
        .then(function(res) {
            if (!res.ok) {
                throw Error(res.statusText);
            }
            return res.json();
        }).then((result) => {
                this.setState({
                    isLoaded: true,
                    plant: result
                });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                this.setState({
                    isLoaded: true,
                    error: error
                });
            });
    }

    mapHandler(event) {
        // Nothing to do, but this function is required
    }

    statesCustomConfig() {
        if (this.state.plant.states == null) {
            return {};
        }

        var statesConfig = {};
        for (var i = 0; i < this.state.plant.states.length; i++) {
            const state = this.state.plant.states[i];
            statesConfig[state.code] = {
                fill: "red",
                clickHandler: (event) => {
                    window.location = '/states/' + state.code;
                }
            };
        }
        return statesConfig;
    }

    render(){
        if (!this.state.isLoaded) {
            return null;
        }

        if (this.state.error) {
            return <Error />;
        }

        const plant = this.state.plant;
        const classification = {
            scientificName: plant.sci_name,
            kingdom: plant.tax_kingdom,
            phylum: plant.tax_phylum,
            theClass: plant.tax_class,
            order: plant.tax_order,
            family: plant.tax_family,
            genus: plant.tax_genus
        };

        return (
            <div>
                <MainNavbar from="plant-page"/>
                <div className="container-fluid">
                  <div className="centered">
                    <h1 id="organism-name">{plant.common_name}</h1>
                    {
                        plant.image_link !== null &&
                        (<img className="principalImage" src={plant.image_link} alt={plant.common_name}/>)
                    }
                    {
                        plant.description !== null &&
                        (<h4 id="description">{plant.description}</h4>)
                    }
                    <h3 className="modelTableHeader">Classification</h3>
                    <Classification classification={classification} />

                    <br />
                    <div><b>Category:</b> {plant.category}</div>
                    <div><b>Category Reason:</b> {plant.category_reason}</div>
                    {
                        plant.threats_notes !== null &&
                        (<div><b>Threats: </b>{plant.threats_notes}</div>)
                    }

                    <h3 className="modelTableHeader">Territories Where {plant.common_name} is Found</h3>
                    <USAMap title="" customize={this.statesCustomConfig()} onClick={this.mapHandler} />
                    <br />

                    <h3 className="modelTableHeader">Animals That Are Also Rated {plant.category}</h3>
                    </div>
                    {
                        plant.animals !== undefined &&
                        (<OrganismCarousel id="animal-carousel" linkBase="/animals/" organisms={plant.animals} />)
                    }
                    <br />
                </div>
            </div>
        );
    }
  }

export default PlantInstance;
