import React from 'react';

function Classification(props) {
    return (
        <table className="table table-bordered w-50">
            <tbody>
                <tr>
                    <th scope="row">Scientific Name</th>
                    <td>{props.classification.scientificName}</td>
                </tr>
                <tr>
                    <th scope="row">Kingdom</th>
                    <td>{props.classification.kingdom}</td>
                </tr>
                <tr>
                    <th scope="row">Phylum</th>
                    <td>{props.classification.phylum}</td>
                </tr>
                <tr>
                    <th scope="row">Class</th>
                    <td>{props.classification.theClass}</td>
                </tr>
                <tr>
                    <th scope="row">Order</th>
                    <td>{props.classification.order}</td>
                </tr>
                <tr>
                    <th scope="row">Family</th>
                    <td>{props.classification.family}</td>
                </tr>
                <tr>
                    <th scope="row">Genus</th>
                    <td>{props.classification.genus}</td>
                </tr>
            </tbody>
        </table>
    );
}

export default Classification;