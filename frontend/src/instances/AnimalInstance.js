import React, { Component } from 'react';
import Classification from './Classification';
import OrganismCarousel from '../carousel/OrganismCarousel.js';
import USAMap from '../usa-map/USAMap.js';
import Error from '../Error.js';
import MainNavbar from '../navbar/MainNavbar';
import './OrganismInstance.css'
import './StateInstance.css'
class AnimalInstance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            animal: null
        };
    }

    componentDidMount() {
        var apiBaseURL = process.env.REACT_APP_API_BASE_URL || "https://api.livingendangerously.me";
        const url = apiBaseURL.concat("/api/animals/").concat(this.props.match.params.id);

        fetch(url)
        .then(function(res) {
            if (!res.ok) {
                throw Error(res.statusText);
            }
            return res.json();
        }).then((result) => {
                this.setState({
                    isLoaded: true,
                    animal: result
                });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                this.setState({
                    isLoaded: true,
                    error: error
                });
            });
    }

    mapHandler(event) {
        // Nothing to do, but this function is required
    }

    statesCustomConfig() {
        var statesConfig = {};
        for (var i = 0; i < this.state.animal.states.length; i++) {
            const state = this.state.animal.states[i];
            statesConfig[state.code] = {
                fill: "red",
                clickHandler: (event) => {
                    window.location = '/states/' + state.code;
                }
            };
        }
        return statesConfig;
    }


    render() {
        if (!this.state.isLoaded) {
            return null;
        }

        if (this.state.error) {
            return <Error />;
        }


        const animal = this.state.animal;
        const classification = {
            scientificName: animal.sci_name,
            kingdom: animal.tax_kingdom,
            phylum: animal.tax_phylum,
            theClass: animal.tax_class,
            order: animal.tax_order,
            family: animal.tax_family,
            genus: animal.tax_genus
        };

        return (
            <div>
                <MainNavbar from="animal-page" />
                <div className="container-fluid centered">
                <h1 id="organism-name">{animal.common_name}</h1>
                {
                    animal.image_link !== null &&
                    (<img className="principalImage" src={animal.image_link} alt={animal.common_name}/>)
                }
                {
                    animal.description !== null &&
                    (<p className="description" id="description">{animal.description}</p>)
                }
                <h3 className="modelTableHeader">Classification</h3>
                <Classification classification={classification} />
                <div className="description"><b>Category:</b> {animal.category}</div>
                <div className="description"><b>Category Reason:</b> {animal.category_reason}</div>
                {
                    animal.threats_notes !== null &&
                    (<div className="description"><b>Threats: </b>{animal.threats_notes}</div>)
                }


                <h3 className="modelTableHeader">Territories Where {animal.common_name} is Found</h3>
                <USAMap title="" customize={this.statesCustomConfig()} onClick={this.mapHandler} />
                <br />
                </div>
                <h3 className="modelTableHeader centered">Plants That Are Also Rated {animal.category}</h3>
                {
                    animal.plants !== undefined &&
                    (<OrganismCarousel className="centered" id="plant-carousel" linkBase="/plants/" organisms={animal.plants} />)
                }
                <br />
            </div>
        );
    }

}
export default AnimalInstance;
