import 'jsdom-global/register';
import React from 'react';
import Enzyme from 'enzyme';
import {mount, shallow} from 'enzyme';
import {expect} from 'chai';
import { BrowserRouter as Router } from 'react-router-dom';
import AnimalModel from '../src/models/AnimalModel';
import PlantModel from '../src/models/PlantModel';
import StateModel from '../src/models/StateModel';
import AnimalInstance from "../src/instances/AnimalInstance"
import PlantInstance from "../src/instances/PlantInstance"
import StateInstance from "../src/instances/StateInstance"
import {getNumberWithCommas} from "../src/instances/StateInstance"
import About from '../src/about/About';
import App from '../src/App';
import Paginator from '../src/models/Paginator';
import ResultModel from '../src/models/ResultModel';
import TeamMember from '../src/about/TeamMember';
import Error from '../src/Error.js';
import Sorter from '../src/models/sort/Sorter';
import StateFilter from '../src/models/filter/StateFilter';
import OrganismFilter from '../src/models/filter/OrganismFilter';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import DropdownButton from 'react-bootstrap';
import Fade from 'react-slideshow-image';
import {Link} from 'react-router-dom';
import USAMap from 'react-usa-map';
import PieChart from 'recharts';
import OrganismCarousel from '../src/carousel/OrganismCarousel.js';
import Adapter from 'enzyme-adapter-react-16';
import {Form, Col, Button, ButtonToolbar} from 'react-bootstrap';
Enzyme.configure({adapter: new Adapter()});


describe('App', function() {
    const wrapper = shallow(<App/>);
    it('should render', function() {
        expect(wrapper.length).to.equal(1);
    });
});

describe('Animal Model', function() {
    let props;
    let mounted_AM;
    const AM = () => {
        mounted_AM = shallow(<AnimalModel{...props}/>);
        return mounted_AM;
    }

    beforeEach(() => {
        props = {
            location: {},
        };
        mounted_AM = undefined;
    });

    it('should render', function() {
        const wrapper = AM();
        expect(wrapper.length).to.equal(1);
    });

    it('should contain a Paginator component', function() {
        const wrapper = AM();
        expect(wrapper.find('Paginator')).to.have.length(1);
    });
});

describe('Plant Model', function() {
    let props;
    let mounted_PM;
    const PM = () => {
        mounted_PM = shallow(<PlantModel{...props}/>);
        return mounted_PM;
    }

    beforeEach(() => {
        props = {
            location: {},
        };
        mounted_PM = undefined;
    });

    it('should render', function() {
        const wrapper = PM();
        expect(wrapper.length).to.equal(1);
    });

    it('should contain a Paginator component', function() {
        const wrapper = PM();
        expect(wrapper.find('Paginator')).to.have.length(1);
    });
});

describe('State Model', function() {
    let props;
    let mounted_SM;
    const SM = () => {
        mounted_SM = shallow(<StateModel{...props}/>);
        return mounted_SM;
    }

    beforeEach(() => {
        props = {
            location: {},
        };
        mounted_SM = undefined;
    });

    it('should render', function() {
        const wrapper = SM();
        expect(wrapper.length).to.equal(1);
    });

    it('should contain a Paginator component', function() {
        const wrapper = SM();
        expect(wrapper.find('Paginator')).to.have.length(1);
    });
});

describe('Paginator', function() {
    let props;
    let mounted_pag;
    const pag = () => {
        mounted_pag = shallow(<Paginator{...props}/>);
        return mounted_pag;
    }

    beforeEach(() => {
        props = {
            currentPage: {},
        };
        mounted_pag = undefined;
    });

    it('should render', function() {
        const wrapper = pag();
        expect(wrapper.length).to.equal(1);
    });

    it('should return 5 pages', function() {
        const wrapper = pag();
        expect(wrapper.instance().getPaginationJSX(1, 2).length).to.equal(6);
    });
});

describe('About', function() {
    let props;
    let mounted_abt;
    const abt = () => {
        mounted_abt = shallow(<About/>);
        return mounted_abt;
    }

    beforeEach(() => {
        mounted_abt = undefined;
    });

    it('should render', function() {
        const wrapper = abt();
        expect(wrapper.length).to.equal(1);
    });

    it('should not contain error', () => {
        const wrapper = abt();
        expect(wrapper.find(Error)).to.have.length(0);
    });
});

describe('Animal Instance', () =>  {
    let props;
    let mounted_AI;

    const AI = () => {
        mounted_AI = shallow(<AnimalInstance{...props} />);
        return mounted_AI;
    }

    beforeEach(() => {
        props = {
            match: {params: {id: 1,}},
        };
        mounted_AI = undefined;
    });

    it('should render', () => {
        const wrapper = AI();
        expect(wrapper.length).to.equal(1);
    });

    it('should have classifications', () => {
        const wrapper = AI();
        wrapper.setState({
            isLoaded: true,
            animal: {
                "category": "EW",
                "common_name": "Heh13",
                "family": "Hominidae",
                "genus": "Homo",
                "id": 1,
                "image": "../../../jackstenglein.jpg",
                "kingdom": "Animalia",
                "order": "Primates",
                "phylum": "Chordata",
                "plants": [
                    {
                        "common_name": "Heh13",
                        "id": 2,
                        "image": "../../../jackstenglein.jpg"
                    }
                ],
                "population_trend": "stable",
                "provinces": [],
                "scientific_name": "Test 13",
                "states": [
                    {
                        "code": "NJ",
                        "country": "usa",
                        "flag_image": null,
                        "gap1_percent": 4,
                        "gap2_percent": 5,
                        "gap3_percent": 6,
                        "gap4_percent": 7,
                        "human_pop": 58300000,
                        "id": 1,
                        "map_image": "../../../florida.png",
                        "name": "New Jersey",
                        "total_land_area": 471900000
                    }
                ],
                "taxonomic_class": "Mammalia"
            }
        });
        expect(wrapper.find('Classification')).to.have.length(1);
    });

    it('should have a USAMap', () => {
        const wrapper = AI();
        wrapper.setState({
            isLoaded: true,
            animal: {
                "category": "EW",
                "common_name": "Heh13",
                "family": "Hominidae",
                "genus": "Homo",
                "id": 1,
                "image": "../../../jackstenglein.jpg",
                "kingdom": "Animalia",
                "order": "Primates",
                "phylum": "Chordata",
                "plants": [
                    {
                        "common_name": "Heh13",
                        "id": 2,
                        "image": "../../../jackstenglein.jpg"
                    }
                ],
                "population_trend": "stable",
                "provinces": [],
                "scientific_name": "Test 13",
                "states": [
                    {
                        "code": "NJ",
                        "country": "usa",
                        "flag_image": null,
                        "gap1_percent": 4,
                        "gap2_percent": 5,
                        "gap3_percent": 6,
                        "gap4_percent": 7,
                        "human_pop": 58300000,
                        "id": 1,
                        "map_image": "../../../florida.png",
                        "name": "New Jersey",
                        "total_land_area": 471900000
                    }
                ],
                "taxonomic_class": "Mammalia"
            }
        });
        expect(wrapper.find(USAMap)).to.have.length(1);
    });

    it('should have a carousel', () => {
        const wrapper = AI();
        wrapper.setState({
            isLoaded: true,
            animal: {
                "category": "EW",
                "common_name": "Heh13",
                "family": "Hominidae",
                "genus": "Homo",
                "id": 1,
                "image": "../../../jackstenglein.jpg",
                "kingdom": "Animalia",
                "order": "Primates",
                "phylum": "Chordata",
                "plants": [
                    {
                        "common_name": "Heh13",
                        "id": 2,
                        "image": "../../../jackstenglein.jpg"
                    }
                ],
                "population_trend": "stable",
                "provinces": [],
                "scientific_name": "Test 13",
                "states": [
                    {
                        "code": "NJ",
                        "country": "usa",
                        "flag_image": null,
                        "gap1_percent": 4,
                        "gap2_percent": 5,
                        "gap3_percent": 6,
                        "gap4_percent": 7,
                        "human_pop": 58300000,
                        "id": 1,
                        "map_image": "../../../florida.png",
                        "name": "New Jersey",
                        "total_land_area": 471900000
                    }
                ],
                "taxonomic_class": "Mammalia"
            }
        });
        expect(wrapper.find('OrganismCarousel')).to.have.length(1);
    });
});

describe('State Instance', () =>  {
    let props;
    let mounted_SI;

    const SI = () => {
        mounted_SI = shallow(<StateInstance{...props} />);
        return mounted_SI;
    }

    beforeEach(() => {
        props = {
            match: {params: {id: 1,}},
        };
        mounted_SI = undefined;
    });

    it('should render', () => {
        const wrapper = SI();
        expect(wrapper.length).to.equal(1);
    });

    it('should not have a USAMap', () => {
        const wrapper = SI();
        expect(wrapper.find(USAMap)).to.have.length(0);
    });

    it('should convert number to readable format', () => {
        getNumberWithCommas(5250325);
    });

    it('should have a PieChart', () => {
        const wrapper = SI();
        wrapper.setState({
          isLoaded: true,
          territory: {
            "animals": [
        {
            "animal_id": 5,
            "common_name": "American Beaver",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/American_Beaver.jpg/220px-American_Beaver.jpg"
        },
        {
            "animal_id": 13,
            "common_name": "Red Squirrel",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/c/c6/Tamiasciurus_hudsonicus_CT.jpg"
        },
        {
            "animal_id": 14,
            "common_name": "Northern Flying Squirrel",
            "image_link": "https://www.nature.ca/notebooks/images/img/132_p_0052_p.jpg"
        },
        {
            "animal_id": 26,
            "common_name": "Hoary Marmot",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/5/5d/Marmota_caligata_%28EH%29.jpg"
        },
        {
            "animal_id": 28,
            "common_name": "Woodchuck",
            "image_link": "https://objects.liquidweb.services/images/201707/mark_johnson_35409687734_f9a27a3982_b.jpg"
        },
        {
            "animal_id": 47,
            "common_name": "Arctic Ground Squirrel",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Spermophilus_parryii_%28eating_mushroom%29.jpg/1200px-Spermophilus_parryii_%28eating_mushroom%29.jpg"
        },
        {
            "animal_id": 153,
            "common_name": "Meadow Jumping Mouse",
            "image_link": "https://www.dlia.org/gallery/DLIA_Pics/Web%20Pics/SpeciesZ/websize/W_Zapus_hudsonius_RB640.jpg"
        },
        {
            "animal_id": 154,
            "common_name": "Western Jumping Mouse",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/4/44/Zapus_princeps.jpg"
        },
        {
            "animal_id": 169,
            "common_name": "Nearctic Brown Lemming",
            "image_link": "https://ak0.picdn.net/shutterstock/videos/14353720/thumb/1.jpg"
        },
        {
            "animal_id": 170,
            "common_name": "Insular Vole",
            "image_link": "https://www.discoverlife.org/IM/I_RB/0000/320/Microtus_pennsylvani"
        },
        {
            "animal_id": 178,
            "common_name": "Singing Vole",
            "image_link": "http://www.biolib.cz/IMG/GAL/20976.jpg"
        },
        {
            "animal_id": 181,
            "common_name": "Root Vole",
            "image_link": "https://iucnredlist-photos.s3.amazonaws.com/medium/286138880.jpg?AWSAccessKeyId=AKIAJIJQNN2N2SMHLZJA&Expires=1562307551&Signature=EHLF01N4y%2BAk4etDY9U4ecJcuIw%3D"
        },
        {
            "animal_id": 183,
            "common_name": "Meadow Vole",
            "image_link": "https://animaldiversity.org/collections/contributors/phil_myers/classic/microtus1/medium.jpg"
        },
        {
            "animal_id": 189,
            "common_name": "Common Muskrat",
            "image_link": "https://c7.alamy.com/comp/DRJA6A/muskrat-ondatra-zibethicus-exotic-introduced-species-native-to-north-DRJA6A.jpg"
        },
        {
            "animal_id": 192,
            "common_name": "Northern Bog Lemming",
            "image_link": "http://iceage.museum.state.il.us/sites/iceage/files/styles/mammal_med/public/mammals/icon/Northern%20Bog%20Lemming%20altered.jpg?itok=XEtKk6Od"
        },
        {
            "animal_id": 196,
            "common_name": "Bushy-tailed Woodrat",
            "image_link": "https://www.fs.fed.us/database/feis/animals/mammal/neci/mammal.jpg"
        },
        {
            "animal_id": 215,
            "common_name": "Northwestern Deermouse",
            "image_link": "https://static.inaturalist.org/photos/6046713/original.jpeg?1484603164"
        },
        {
            "animal_id": 217,
            "common_name": "North American Deermouse",
            "image_link": "http://calphotos.berkeley.edu/imgs/512x768/1073_3283/3181/0030.jpeg"
        },
        {
            "animal_id": 238,
            "common_name": "Collared Pika",
            "image_link": "https://static.inaturalist.org/photos/2281300/original.jpg?1439954536"
        },
        {
            "animal_id": 252,
            "common_name": "Snowshoe Hare",
            "image_link": "https://farm6.staticflickr.com/5183/5734318760_f67445b4b1_o.jpg"
        }
    ],
    "code": "AK",
    "flag_image": null,
    "gap1": "31",
    "gap2": "4",
    "gap3": "20",
    "human_pop": "741522",
    "land_area": "425,845,904",
    "map_image": "https://s3.us-east-2.amazonaws.com/living-endangerously-territory-pictures/alaska.png",
    "name": "Alaska",
    "plants": [
        {
            "common_name": "Arctic Harebell",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4135+1341951836.JPG",
            "plant_id": 1
        },
        {
            "common_name": "Brown Bent",
            "image_link": "http://www.boldsystems.org/pics/POWNA/NMW3156+1328486136.jpg",
            "plant_id": 3
        },
        {
            "common_name": "Douglas' Spiraea",
            "image_link": "http://plants.usda.gov/gallery/large/spme2_001_lhp.jpg",
            "plant_id": 5
        },
        {
            "common_name": "Alaska Knotweed",
            "image_link": "http://www.boldsystems.org/pics/VASCB/CCDB-23955-A03+1441918158.JPG",
            "plant_id": 10
        },
        {
            "common_name": "Whorled Lousewort",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/CCDB-21230-B10_AK_Pedi_vert+1395945974.jpg",
            "plant_id": 12
        },
        {
            "common_name": "Hairy Arnica",
            "image_link": "http://www.boldsystems.org/pics/VPSBC/V238951+1385420942.jpg",
            "plant_id": 17
        },
        {
            "common_name": "Hairy Arnica",
            "image_link": "http://www.boldsystems.org/pics/VPSBC/V238951+1385420942.jpg",
            "plant_id": 18
        },
        {
            "common_name": "Jakutsk Snow-parsley",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_3549+1343759988.JPG",
            "plant_id": 19
        },
        {
            "common_name": "Western Meadowrue",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_5107+1343075466.JPG",
            "plant_id": 20
        },
        {
            "common_name": "Smooth Wild Rye",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/CCDB-18358-H09+1395946536.JPG",
            "plant_id": 23
        },
        {
            "common_name": "Mackenzie's Water-hemlock",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_3547+1343759976.JPG",
            "plant_id": 25
        },
        {
            "common_name": "Small-head Clover",
            "image_link": "http://calphotos.berkeley.edu/imgs/512x768/0000_0000/1109/1549.jpeg",
            "plant_id": 32
        },
        {
            "common_name": "Boreal Bedstraw",
            "image_link": "http://plants.usda.gov/gallery/large/gaka_001_lvd.jpg",
            "plant_id": 34
        },
        {
            "common_name": "American Silverberry",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4311+1342022556.JPG",
            "plant_id": 35
        },
        {
            "common_name": "Lenticular Sedge",
            "image_link": "http://plants.usda.gov/gallery/large/cake2_001_lvp.jpg",
            "plant_id": 36
        },
        {
            "common_name": "Copper-flower",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4316+1342022570.JPG",
            "plant_id": 49
        },
        {
            "common_name": "Arrow-leaf Groundsel",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_3793+1341948654.JPG",
            "plant_id": 51
        },
        {
            "common_name": "Nodding Saxifrage",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_5315+1343231498.JPG",
            "plant_id": 56
        },
        {
            "common_name": "Red-osier Dogwood",
            "image_link": "http://www.boldsystems.org/pics/SDH/CCDB-24940-H03_h199128+1426264776.jpg",
            "plant_id": 63
        },
        {
            "common_name": "Boreal Stitchwort",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4197+1342022142.JPG",
            "plant_id": 71
        }
    ],
    "unprotected": "45"
          }
        });
        expect(wrapper.find('PieChart')).to.have.length(1);
    });

    it('should have two carousels', () => {
        const wrapper = SI();
        wrapper.setState({
          isLoaded: true,
          territory: {
            "animals": [
        {
            "animal_id": 5,
            "common_name": "American Beaver",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/American_Beaver.jpg/220px-American_Beaver.jpg"
        },
        {
            "animal_id": 13,
            "common_name": "Red Squirrel",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/c/c6/Tamiasciurus_hudsonicus_CT.jpg"
        },
        {
            "animal_id": 14,
            "common_name": "Northern Flying Squirrel",
            "image_link": "https://www.nature.ca/notebooks/images/img/132_p_0052_p.jpg"
        },
        {
            "animal_id": 26,
            "common_name": "Hoary Marmot",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/5/5d/Marmota_caligata_%28EH%29.jpg"
        },
        {
            "animal_id": 28,
            "common_name": "Woodchuck",
            "image_link": "https://objects.liquidweb.services/images/201707/mark_johnson_35409687734_f9a27a3982_b.jpg"
        },
        {
            "animal_id": 47,
            "common_name": "Arctic Ground Squirrel",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Spermophilus_parryii_%28eating_mushroom%29.jpg/1200px-Spermophilus_parryii_%28eating_mushroom%29.jpg"
        },
        {
            "animal_id": 153,
            "common_name": "Meadow Jumping Mouse",
            "image_link": "https://www.dlia.org/gallery/DLIA_Pics/Web%20Pics/SpeciesZ/websize/W_Zapus_hudsonius_RB640.jpg"
        },
        {
            "animal_id": 154,
            "common_name": "Western Jumping Mouse",
            "image_link": "https://upload.wikimedia.org/wikipedia/commons/4/44/Zapus_princeps.jpg"
        },
        {
            "animal_id": 169,
            "common_name": "Nearctic Brown Lemming",
            "image_link": "https://ak0.picdn.net/shutterstock/videos/14353720/thumb/1.jpg"
        },
        {
            "animal_id": 170,
            "common_name": "Insular Vole",
            "image_link": "https://www.discoverlife.org/IM/I_RB/0000/320/Microtus_pennsylvani"
        },
        {
            "animal_id": 178,
            "common_name": "Singing Vole",
            "image_link": "http://www.biolib.cz/IMG/GAL/20976.jpg"
        },
        {
            "animal_id": 181,
            "common_name": "Root Vole",
            "image_link": "https://iucnredlist-photos.s3.amazonaws.com/medium/286138880.jpg?AWSAccessKeyId=AKIAJIJQNN2N2SMHLZJA&Expires=1562307551&Signature=EHLF01N4y%2BAk4etDY9U4ecJcuIw%3D"
        },
        {
            "animal_id": 183,
            "common_name": "Meadow Vole",
            "image_link": "https://animaldiversity.org/collections/contributors/phil_myers/classic/microtus1/medium.jpg"
        },
        {
            "animal_id": 189,
            "common_name": "Common Muskrat",
            "image_link": "https://c7.alamy.com/comp/DRJA6A/muskrat-ondatra-zibethicus-exotic-introduced-species-native-to-north-DRJA6A.jpg"
        },
        {
            "animal_id": 192,
            "common_name": "Northern Bog Lemming",
            "image_link": "http://iceage.museum.state.il.us/sites/iceage/files/styles/mammal_med/public/mammals/icon/Northern%20Bog%20Lemming%20altered.jpg?itok=XEtKk6Od"
        },
        {
            "animal_id": 196,
            "common_name": "Bushy-tailed Woodrat",
            "image_link": "https://www.fs.fed.us/database/feis/animals/mammal/neci/mammal.jpg"
        },
        {
            "animal_id": 215,
            "common_name": "Northwestern Deermouse",
            "image_link": "https://static.inaturalist.org/photos/6046713/original.jpeg?1484603164"
        },
        {
            "animal_id": 217,
            "common_name": "North American Deermouse",
            "image_link": "http://calphotos.berkeley.edu/imgs/512x768/1073_3283/3181/0030.jpeg"
        },
        {
            "animal_id": 238,
            "common_name": "Collared Pika",
            "image_link": "https://static.inaturalist.org/photos/2281300/original.jpg?1439954536"
        },
        {
            "animal_id": 252,
            "common_name": "Snowshoe Hare",
            "image_link": "https://farm6.staticflickr.com/5183/5734318760_f67445b4b1_o.jpg"
        }
    ],
    "code": "AK",
    "flag_image": null,
    "gap1": "31",
    "gap2": "4",
    "gap3": "20",
    "human_pop": "741522",
    "land_area": "425,845,904",
    "map_image": "https://s3.us-east-2.amazonaws.com/living-endangerously-territory-pictures/alaska.png",
    "name": "Alaska",
    "plants": [
        {
            "common_name": "Arctic Harebell",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4135+1341951836.JPG",
            "plant_id": 1
        },
        {
            "common_name": "Brown Bent",
            "image_link": "http://www.boldsystems.org/pics/POWNA/NMW3156+1328486136.jpg",
            "plant_id": 3
        },
        {
            "common_name": "Douglas' Spiraea",
            "image_link": "http://plants.usda.gov/gallery/large/spme2_001_lhp.jpg",
            "plant_id": 5
        },
        {
            "common_name": "Alaska Knotweed",
            "image_link": "http://www.boldsystems.org/pics/VASCB/CCDB-23955-A03+1441918158.JPG",
            "plant_id": 10
        },
        {
            "common_name": "Whorled Lousewort",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/CCDB-21230-B10_AK_Pedi_vert+1395945974.jpg",
            "plant_id": 12
        },
        {
            "common_name": "Hairy Arnica",
            "image_link": "http://www.boldsystems.org/pics/VPSBC/V238951+1385420942.jpg",
            "plant_id": 17
        },
        {
            "common_name": "Hairy Arnica",
            "image_link": "http://www.boldsystems.org/pics/VPSBC/V238951+1385420942.jpg",
            "plant_id": 18
        },
        {
            "common_name": "Jakutsk Snow-parsley",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_3549+1343759988.JPG",
            "plant_id": 19
        },
        {
            "common_name": "Western Meadowrue",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_5107+1343075466.JPG",
            "plant_id": 20
        },
        {
            "common_name": "Smooth Wild Rye",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/CCDB-18358-H09+1395946536.JPG",
            "plant_id": 23
        },
        {
            "common_name": "Mackenzie's Water-hemlock",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_3547+1343759976.JPG",
            "plant_id": 25
        },
        {
            "common_name": "Small-head Clover",
            "image_link": "http://calphotos.berkeley.edu/imgs/512x768/0000_0000/1109/1549.jpeg",
            "plant_id": 32
        },
        {
            "common_name": "Boreal Bedstraw",
            "image_link": "http://plants.usda.gov/gallery/large/gaka_001_lvd.jpg",
            "plant_id": 34
        },
        {
            "common_name": "American Silverberry",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4311+1342022556.JPG",
            "plant_id": 35
        },
        {
            "common_name": "Lenticular Sedge",
            "image_link": "http://plants.usda.gov/gallery/large/cake2_001_lvp.jpg",
            "plant_id": 36
        },
        {
            "common_name": "Copper-flower",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4316+1342022570.JPG",
            "plant_id": 49
        },
        {
            "common_name": "Arrow-leaf Groundsel",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_3793+1341948654.JPG",
            "plant_id": 51
        },
        {
            "common_name": "Nodding Saxifrage",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_5315+1343231498.JPG",
            "plant_id": 56
        },
        {
            "common_name": "Red-osier Dogwood",
            "image_link": "http://www.boldsystems.org/pics/SDH/CCDB-24940-H03_h199128+1426264776.jpg",
            "plant_id": 63
        },
        {
            "common_name": "Boreal Stitchwort",
            "image_link": "http://www.boldsystems.org/pics/BBYUK/IMG_4197+1342022142.JPG",
            "plant_id": 71
        }
    ],
    "unprotected": "45"
          }
        });
        expect(wrapper.find('OrganismCarousel')).to.have.length(2);
    });
});

describe('Plant Instance', () =>  {
    let props;
    let mounted_PI;

    const PI = () => {
        mounted_PI = shallow(<PlantInstance{...props} />);
        return mounted_PI;
    }

    beforeEach(() => {
        props = {
            match: {params: {id: 2,}},
        };
        mounted_PI = undefined;
    });

    it('should render', () => {
        const wrapper = PI();
        expect(wrapper.length).to.equal(1);
    });

    it('should have classifications', () => {
      const wrapper = PI();
      wrapper.setState({
          isLoaded: true,
          plant: {
              "animals": [],
              "category": "EW",
              "common_name": "Heh13",
              "family": "Hominidae",
              "genus": "Homo",
              "id": 2,
              "image": "../../../jackstenglein.jpg",
              "kingdom": "Plantae",
              "order": "Primates",
              "phylum": "Chordata",
              "population_trend": "stable",
              "provinces": [],
              "scientific_name": "Test 14",
              "states": [
                  {
                      "code": "NJ",
                      "country": "usa",
                      "flag_image": null,
                      "gap1_percent": 4,
                      "gap2_percent": 5,
                      "gap3_percent": 6,
                      "gap4_percent": 7,
                      "human_pop": 58300000,
                      "id": 1,
                      "map_image": "../../../florida.png",
                      "name": "New Jersey",
                      "total_land_area": 471900000
                  },
              ],
              "taxonomic_class": "Mammalia",
          }
      });
      expect(wrapper.find('Classification')).to.have.length(1);
    });

    it('should have carousel', () => {
      const wrapper = PI();
      wrapper.setState({
          isLoaded: true,
          plant: {
              "animals": [],
              "category": "EW",
              "common_name": "Heh13",
              "family": "Hominidae",
              "genus": "Homo",
              "id": 2,
              "image": "../../../jackstenglein.jpg",
              "kingdom": "Plantae",
              "order": "Primates",
              "phylum": "Chordata",
              "population_trend": "stable",
              "provinces": [],
              "scientific_name": "Test 14",
              "states": [
                  {
                      "code": "NJ",
                      "country": "usa",
                      "flag_image": null,
                      "gap1_percent": 4,
                      "gap2_percent": 5,
                      "gap3_percent": 6,
                      "gap4_percent": 7,
                      "human_pop": 58300000,
                      "id": 1,
                      "map_image": "../../../florida.png",
                      "name": "New Jersey",
                      "total_land_area": 471900000
                  },
              ],
              "taxonomic_class": "Mammalia",
          }
      });
      expect(wrapper.find('OrganismCarousel')).to.have.length(1);
    });

    it('should have a USA Map', () => {
        const wrapper = PI();
        wrapper.setState({
            isLoaded: true,
            plant: {
                "animals": [],
                "category": "EW",
                "common_name": "Heh13",
                "family": "Hominidae",
                "genus": "Homo",
                "id": 2,
                "image": "../../../jackstenglein.jpg",
                "kingdom": "Plantae",
                "order": "Primates",
                "phylum": "Chordata",
                "population_trend": "stable",
                "provinces": [],
                "scientific_name": "Test 14",
                "states": [
                    {
                        "code": "NJ",
                        "country": "usa",
                        "flag_image": null,
                        "gap1_percent": 4,
                        "gap2_percent": 5,
                        "gap3_percent": 6,
                        "gap4_percent": 7,
                        "human_pop": 58300000,
                        "id": 1,
                        "map_image": "../../../florida.png",
                        "name": "New Jersey",
                        "total_land_area": 471900000
                    },
                ],
                "taxonomic_class": "Mammalia",
            }
        });
        wrapper.update();
        expect(wrapper.find(USAMap)).to.have.length(1);
    });
});

describe('ResultModel', () => {
    let props;
    let mounted_RM;

    const mountRM = () => {
        mounted_RM = shallow(<ResultModel{...props}/>);
        return mounted_RM;
    }

    beforeEach(() => {
        props = {
          location: {},
        };
        mounted_RM = undefined;
    });

    it('should render', () => {
      const wrapper = mountRM();
      expect(wrapper.length).to.equal(1);
    });

    it('should have three individual tabs', () => {
      const wrapper = mountRM();
      expect(wrapper.find(Tab)).to.have.length(3);
    });

    it('should have overarching Tabs component', () => {
      const wrapper = mountRM();
      expect(wrapper.find(Tabs)).to.have.length(1);
    })
});

describe('StateFilter', () => {
    let props;
    let mounted_SF;

    const mountSF = () => {
        mounted_SF = shallow(<StateFilter{...props}/>);
        return mounted_SF;
    }

    beforeEach(() => {
        props = {};
        mounted_SF = undefined;
    });

    it('should render', () => {
        const wrapper = mountSF();
        expect(wrapper.length).to.equal(1);
    });

    it('should have a title', () => {
        const wrapper = mountSF();
        expect(wrapper.find('h3')).to.have.length(1);
    });

    it('should have StateFilterSections', () => {
        const wrapper = mountSF();
        expect(wrapper.find('StateFilterSection')).to.have.length(6);
    });
});

describe('OrganismFilter', () => {
    let props;
    let mounted_OF;

    const mountOF = () => {
        mounted_OF = shallow(<OrganismFilter{...props}/>);
        return mounted_OF;
    }

    beforeEach(() => {
        props = {};
        mounted_OF = undefined;
    });

    it('should render', () => {
        const wrapper = mountOF();
        expect(wrapper.length).to.equal(1);
    });

    it('should have a title', () => {
        const wrapper = mountOF();
        expect(wrapper.find('h3')).to.have.length(4);
    });

    it('should have checkboxes', () => {
        const wrapper = mountOF();
        expect(wrapper.find(Form.Check)).to.have.length(110);
    });

    it('should have apply buttons', () => {
        const wrapper = mountOF();
        expect(wrapper.find(Button)).to.have.length(3);
    });

    it('should have reset buttons', () => {
        const wrapper = mountOF();
        wrapper.setState({categoriesApplied: true, statesApplied: true});
        expect(wrapper.find(Button)).to.have.length(5);
    });
});

describe('OrganismCarousel', () => {
  let props;
  let mounted_OC;

  const mountOC = () => {
      mounted_OC = shallow(<OrganismCarousel{...props}/>);
      return mounted_OC;
  }

  beforeEach(() => {
    props = {
      organism: {},
      linkBase: {},
    };
    mounted_OC = undefined;
  });

  it('should render', () => {
    const wrapper = mountOC();
    expect(wrapper.length).to.equal(1);
  });
});

describe('Sorter', () => {
    let props;
    let mountedSorter;

    const mountSorter = () => {
        mountedSorter = shallow(<Sorter{...props}/>);
        return mountedSorter;
    }

    beforeEach(() => {
        props = {
            fields: [
                {"key": "test1", "name": "Test 1"},
                {"key": "test2", "name": "Test 2"}
            ]
        };
        mountedSorter = undefined;
    });

    it('should render', () => {
        const wrapper = mountSorter();
        expect(wrapper.length).to.equal(1);
    });

    it('should have a form', () => {
        const wrapper = mountSorter();
        expect(wrapper.find(Form).length).to.equal(1);
    });

    it('should have checkboxes', () => {
        const wrapper = mountSorter();
        expect(wrapper.find(Form.Check).length).to.equal(4);
    });

    it('should have an apply button', () => {
        const wrapper = mountSorter();
        expect(wrapper.find(Button).length).to.equal(1);
    });

    it('should have a reset button', () => {
        const wrapper = mountSorter();
        wrapper.setState({applied: true});
        expect(wrapper.find(Button).length).to.equal(2);
    });
});


/*describe('Search', () =>  {
    let props;
    let mounted_search;

    const search = () => {
        mounted_search = shallow(<Search{...props} />);
        return mounted_PI;
    }

    beforeEach(() => {
        mounted_search = undefined;
    });

    it('should render', () => {
        const wrapper = search();
        expect(wrapper.length).to.equal(1);
    });
});*/
