import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


class LivingEndangerouslyTestCase(unittest.TestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(options=chrome_options)

    def test_aaa_gitlab_ci(self):
        """ Does not actually test anything.

        This test's only purpose is to run as the first test on GitLab CI, which
        runs tests alphabetically. For some reason, the first test always times out on
        GitLab CI. This test is our work around.
        """

        self.driver.get("http://localhost:3000/about")
        try:
            element = WebDriverWait(self.driver, 20).until(
                EC.presence_of_element_located((By.ID, "non-existent-id"))
            )
        except TimeoutException:
            pass

    def test_about_page(self):
        """ Tests the about page.

        Makes sure that all required elements of the about page are actually on the page.
        """

        self.driver.get("http://localhost:3000/about")
        try:
            element = WebDriverWait(self.driver, 20).until(
                EC.presence_of_element_located((By.ID, "gitlab-stats-loaded"))
            )
        except TimeoutException:
            elements = self.driver.find_elements_by_id("error")
            if len(elements) > 0:
                self.fail("About page encountered error: " + elements[0].text)
            self.fail("About page failed to load stats from GitLab.")

        # Check the About Us section exists
        elems = self.driver.find_elements_by_class_name("about-us")
        self.assertEqual(len(elems), 1)

        # Check that the team members exist
        elems = self.driver.find_elements_by_class_name("about")
        self.assertEqual(len(elems), 5)

        # Check that the proper headers exist
        elems = self.driver.find_elements_by_tag_name("li")
        self.assertEqual(len(elems), 5)
        expected_headers = [
            "Meet the team",
            "Our data",
            "Our tools",
            "GitLab stats",
            "Other links",
        ]
        self.assertEqual(list(map(lambda elem: elem.text, elems)), expected_headers)

    def test_pagination_nav_page1(self):
        """ Tests the paging navigation box for the model pages when on page 1.

        This function checks that the active class is correct on the paging navigator,
        as well as the values of the other boxes.
        """

        self.driver.get("http://localhost:3000/states")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "page-navigator"))
            )
        except TimeoutException:
            self.fail("State model page failed to load.")

        # Check that the page navigator exists
        elems = self.driver.find_elements_by_class_name("page-item")
        self.assertEqual(len(elems), 7)

        # Check the start button exists and is enabled
        it = iter(elems)
        start_link = next(it)
        self.assertEqual("Start", start_link.text)
        self.assertFalse(
            "disabled" in start_link.get_attribute("class"),
            "Start page link is disabled.",
        )

        # We are on page 1, so the previous link should be disabled
        prev_link = next(it)
        self.assertTrue(
            "disabled" in prev_link.get_attribute("class"),
            "'Prev' link in page navigator not disabled.",
        )
        self.assertEqual("Prev", prev_link.text)

        # Check the link to the current page
        current_link = next(it)
        self.assertTrue(
            "active" in current_link.get_attribute("class"),
            "Current page link does not have 'active' class.",
        )
        self.assertEqual("1", current_link.text)

        # Check the remaining links
        for link in it:
            self.assertFalse(
                "active" in link.get_attribute("class"),
                "Non-current page link has 'active' class.",
            )
            self.assertFalse(
                "disabled" in link.get_attribute("class"),
                "Non-current page link is disabled.",
            )

    def test_pagination_nav_general(self):
        """ Tests the paging navigation box for the model pages when not on page 1.

        This function checks that the active class is correct on the paging navigator,
        as well as the values of the other boxes.
        """

        self.driver.get("http://localhost:3000/states?page=2")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "page-navigator"))
            )
        except TimeoutException:
            self.fail("State model page failed to load.")

        # Check that the page navigator exists
        elems = self.driver.find_elements_by_class_name("page-item")
        self.assertEqual(len(elems), 7)

        # The previous link should be enabled
        self.assertFalse(
            "disabled" in elems[1].get_attribute("class"),
            "'Prev' link is disabled but should not be.",
        )

        # The link to page 2 should be active
        self.assertTrue(
            "active" in elems[3].get_attribute("class"),
            "Current page link does not have 'active' class.",
        )

        # Check text values
        self.assertEqual("Start", elems[0].text)
        self.assertEqual("Prev", elems[1].text)
        self.assertEqual("1", elems[2].text)
        self.assertEqual("2", elems[3].text)
        self.assertEqual("3", elems[4].text)
        self.assertEqual("Next", elems[5].text)
        self.assertEqual("End", elems[6].text)

    def test_pagination_nav_start_end(self):
        """ Verifies that the paginator's start and end buttons work correctly. """
        self.driver.get("http://localhost:3000/animals")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "loading-finished"))
            )
        except TimeoutException:
            self.fail("Animal model page failed to load.")

        # Click the end button
        page_links = self.driver.find_elements_by_class_name("page-item")
        self.assertEqual(len(page_links), 6)
        self.assertEqual(page_links[5].text, "End")
        page_links[5].click()
        self.assertEqual(page_links[3].text, "2")
        self.assertTrue(
            "active" in page_links[3].get_attribute("class"),
            "Second page is not active.",
        )
        self.assertEqual(page_links[4].text, "Next")
        self.assertTrue(
            "disabled" in page_links[4].get_attribute("class"),
            "Next button is not disabled.",
        )

        # Click the start button
        self.assertEqual(page_links[0].text, "Start")
        page_links[0].click()
        self.assertEqual(page_links[2].text, "1")
        self.assertTrue(
            "active" in page_links[2].get_attribute("class"),
            "First page is not active.",
        )
        self.assertEqual(page_links[1].text, "Prev")
        self.assertTrue(
            "disabled" in page_links[1].get_attribute("class"),
            "Prev button is not disabled.",
        )

    def test_territory_instance_page(self):
        """ Verifies the layout of the territory instance page. """

        self.driver.get("http://localhost:3000/states/Test%2013")

        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "territory-name"))
            )
        except TimeoutException:
            self.fail("State instance page failed to load.")

        # Check that the top image is displayed
        elems = self.driver.find_elements_by_class_name("principalImage")
        self.assertEqual(len(elems), 1)

        # Check that the name of the territory is displayed, will throw an error if not found
        title = self.driver.find_element_by_id("territory-name")
        state_name = title.text

        # Check that the right headers are displayed on the page
        elems = self.driver.find_elements_by_tag_name("h3")
        self.assertEqual(len(elems), 5)
        expected_headers = [
            "Key Statistics",
            "GAP Protected Area Percentages ?",
            "Animals in " + state_name,
            "Plants in " + state_name,
            "GAP Status Codes",
        ]
        self.assertEqual(list(map(lambda elem: elem.text, elems)), expected_headers)

    def check_organism_instance_page(self, link):
        """ Verifies the layout of the animal or plant instance page. """

        self.driver.get(link)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "organism-name"))
            )
        except TimeoutException:
            self.fail("Organism instance page failed to load.")

        # Check that the top image is displayed
        elems = self.driver.find_elements_by_class_name("principalImage")
        self.assertEqual(len(elems), 1)

        # Check that the name of the organism is displayed
        title = self.driver.find_element_by_id("organism-name")
        name = title.text

        # Check that the right headers are displayed on the page
        elems = self.driver.find_elements_by_tag_name("h3")
        self.assertEqual(len(elems), 3)
        self.assertEqual("Classification", elems[0].text)
        self.assertEqual("Territories Where " + name + " is Found", elems[1].text)
        self.assertTrue("That Are Also Rated" in elems[2].text)

        # Check that the map exists
        elems = self.driver.find_elements_by_tag_name("svg")
        self.assertEqual(len(elems), 1)

    def test_animal_instance_page(self):
        self.check_organism_instance_page("http://localhost:3000/animals/1")

    def test_plant_instance_page(self):
        self.check_organism_instance_page("http://localhost:3000/plants/2")

    def check_model_page(self, link):
        """ Verifies that the model page contains at least one element and at most 9 elements. """

        self.driver.get(link)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "page-navigator"))
            )
        except TimeoutException:
            self.fail("Model page failed to load.")

        elems = self.driver.find_elements_by_class_name("paginationCard")
        self.assertTrue(len(elems) > 0, msg="No items were listed.")
        self.assertTrue(len(elems) <= 9, msg="More than 9 items were listed.")

    def test_animal_model_page(self):
        self.check_model_page("http://localhost:3000/animals")

    def test_plant_model_page(self):
        self.check_model_page("http://localhost:3000/plants")

    def test_state_model_page(self):
        self.check_model_page("http://localhost:3000/states")

    def test_navbar(self):
        """ Verifies that the navbar renders with the proper options. """

        self.driver.get("http://localhost:3000")
        navbar = self.driver.find_element_by_id("nav")
        options = ["LIVING ENDANGEROUSLY", "ANIMALS", "PLANTS", "STATES", "ABOUT"]
        for option in options:
            self.assertTrue(
                option in navbar.text, msg="Option '" + option + "' not in navbar."
            )

    def test_splash_page(self):
        """ Verifies that the splash page renders correctly. """

        self.driver.get("http://localhost:3000")
        body = self.driver.find_element_by_id("front-page")

        body.find_element_by_id("goat")
        body.find_element_by_id("statement")
        body.find_element_by_id("button")
        body.find_element_by_id("quote-page")
        body.find_element_by_id("quote-text")
        body.find_element_by_id("quote-picture")
        body.find_element_by_id("model-buttons")

    def test_states_filter_section(self):
        """ Verifies that the State model page has a section to search, sort and filter. """

        self.driver.get("http://localhost:3000/states")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "loading-finished"))
            )
        except TimeoutException:
            self.fail("Model page failed to load.")

        filter_section = self.driver.find_elements_by_class_name("border-right")[0]

        # Verify the main headers
        section_headers = filter_section.find_elements_by_tag_name("h3")
        expected_headers = ["Search States", "Sort Results", "Filter Results"]
        actual_headers = [header.text for header in section_headers]
        self.assertEqual(actual_headers, expected_headers)

        # Verify the subheaders in the filter section
        filter_subheaders = filter_section.find_elements_by_tag_name("h5")
        expected_headers = [
            "Human Population",
            "Total Land Area",
            "GAP 1 Area",
            "GAP 2 Area",
            "GAP 3 Area",
            "Unprotected Area",
        ]
        actual_headers = [header.text for header in filter_subheaders]
        self.assertEqual(actual_headers, expected_headers)

        # Verify the number of checkboxes
        checkboxes = filter_section.find_elements_by_class_name("form-check-input")
        self.assertEqual(len(checkboxes), 9)

        # Verify the number of "Apply" buttons
        apply_buttons = filter_section.find_elements_by_tag_name("button")
        self.assertEqual(len(apply_buttons), 8)

        # Verify the search bar
        search_bar = filter_section.find_elements_by_class_name("form-control")[0]
        self.assertEqual(search_bar.get_attribute("placeholder"), "Search")

    def check_organism_filter_section(self, url, model):
        """ Verifies that the given model page has an organism filter section. """
        self.driver.get(url)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "loading-finished"))
            )
        except TimeoutException:
            self.fail("Model page failed to load.")

        filter_section = self.driver.find_elements_by_class_name("border-right")[0]

        # Verify the main headers, add 3 empty for the expand icons
        section_headers = filter_section.find_elements_by_tag_name("h3")
        expected_headers = [
            "Search " + model,
            "Sort Results",
            "Filter Results",
            "",
            "",
            "",
        ]
        actual_headers = [header.text for header in section_headers]
        self.assertEqual(actual_headers, expected_headers)

        # Verify the subheaders in the filter section
        filter_subheaders = filter_section.find_elements_by_tag_name("h5")
        expected_headers = ["Conservation Category", "State", "Habitat"]
        actual_headers = [header.text for header in filter_subheaders]
        self.assertEqual(actual_headers, expected_headers)

        # Verify the number of checkboxes
        checkboxes = filter_section.find_elements_by_class_name("form-check-input")
        self.assertEqual(len(checkboxes), 115)

        # Verify the number of "Apply" buttons
        apply_buttons = filter_section.find_elements_by_tag_name("button")
        self.assertEqual(len(apply_buttons), 5)

        # Verify the search bar
        search_bar = filter_section.find_elements_by_class_name("form-control")[0]
        self.assertEqual(search_bar.get_attribute("placeholder"), "Search")

    def test_animals_filter_section(self):
        self.check_organism_filter_section("http://localhost:3000/animals", "Animals")

    def test_plants_filter_section(self):
        self.check_organism_filter_section("http://localhost:3000/plants", "Plants")

    def test_search_bar(self):
        """ Verifies that a search bar renders in the nav bar and that it sends you to the right page. """
        self.driver.get("http://localhost:3000")
        navbar = self.driver.find_element_by_id("nav")
        search_form = navbar.find_elements_by_tag_name("form")[0]
        search_input = search_form.find_elements_by_tag_name("input")[0]
        search_button = search_form.find_elements_by_tag_name("a")[0]
        self.assertEqual(search_button.text, "Search")
        self.assertEqual(search_input.get_attribute("placeholder"), "Search Site")

        # Type in the search bar and click search
        search_input.click()
        search_input.send_keys("Test")
        search_button.click()
        self.assertEqual(
            self.driver.current_url, "http://localhost:3000/results?search=Test"
        )

        # Do the same thing but push enter instead of clicking the button
        self.driver.get("http://localhost:3000")
        search_input = self.driver.find_elements_by_tag_name("input")[0]
        search_input.send_keys("Test", Keys.ENTER)
        self.assertEqual(
            self.driver.current_url, "http://localhost:3000/results?search=Test"
        )

    def test_search_render(self):
        """ Verifies that the results page renders correctly after searching. """

        self.driver.get("http://localhost:3000/results?search=Test")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "loading-finished"))
            )
        except TimeoutException:
            self.fail("Model page failed to load.")

        self.assertEqual(
            self.driver.find_element_by_id("result-search-text").text,
            'Results for "Test":',
        )

        # Check that the tabs display correctly
        animals_tab = self.driver.find_element_by_id("controlled-tabs-tab-animals")
        self.assertEqual(animals_tab.text, "Animals")
        self.assertTrue(
            "active" in animals_tab.get_attribute("class"), "Animals tab is not active."
        )

        plants_tab = self.driver.find_element_by_id("controlled-tabs-tab-plants")
        self.assertEqual(plants_tab.text, "Plants")
        self.assertFalse(
            "active" in plants_tab.get_attribute("class"),
            "Plants tab is active too early.",
        )
        plants_tab.click()
        self.assertTrue(
            "active" in plants_tab.get_attribute("class"), "Plants tab is not active."
        )
        self.assertFalse(
            "active" in animals_tab.get_attribute("class"),
            "Animals tab is still active.",
        )

        states_tab = self.driver.find_element_by_id("controlled-tabs-tab-states")
        self.assertEqual(states_tab.text, "States")
        self.assertFalse(
            "active" in states_tab.get_attribute("class"),
            "States tab is active too early.",
        )
        states_tab.click()
        self.assertTrue(
            "active" in states_tab.get_attribute("class"), "States tab is not active."
        )
        self.assertFalse(
            "active" in plants_tab.get_attribute("class"), "Plants tab is still active."
        )
        self.assertFalse(
            "active" in animals_tab.get_attribute("class"),
            "Animals tab is still active.",
        )

    def test_search_results(self):
        """ Verifies that the results page displays the correct results. """
        self.driver.get("http://localhost:3000/results?search=10")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "loading-finished"))
            )
        except TimeoutException:
            self.fail("Model page failed to load.")

        self.assertEqual(
            self.driver.find_element_by_id("result-search-text").text,
            'Results for "10":',
        )

        # There shouldn't be any animals
        self.assertEqual(
            self.driver.find_element_by_id("result-none").text, "No results found."
        )

        # There should be one plant
        plants_tab = self.driver.find_element_by_id("controlled-tabs-tab-plants")
        plants_tab.click()
        cards = self.driver.find_element_by_id(
            "controlled-tabs-tabpane-plants"
        ).find_elements_by_class_name("card-body")
        self.assertEqual(len(cards), 1)
        highlighted = cards[0].find_elements_by_tag_name("mark")
        self.assertEqual(len(highlighted), 1)
        self.assertEqual(highlighted[0].text, "10")

        # There should be one state
        states_tab = self.driver.find_element_by_id("controlled-tabs-tab-states")
        states_tab.click()
        cards = self.driver.find_element_by_id(
            "controlled-tabs-tabpane-states"
        ).find_elements_by_class_name("card-body")
        self.assertEqual(len(cards), 1)
        highlighted = cards[0].find_elements_by_tag_name("mark")
        self.assertEqual(len(highlighted), 2)
        self.assertEqual(highlighted[0].text, "10")
        self.assertEqual(highlighted[1].text, "10")

    def test_state_link_carousels(self):
        """ Verifies that the state instance page shows a carousel for animals and plants. """

        self.driver.get("http://localhost:3000/states/Test%2013")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "loading-finished"))
            )
        except TimeoutException:
            self.fail("Instance page failed to load.")

        carousels = self.driver.find_elements_by_class_name("react-slideshow-container")
        self.assertEqual(len(carousels), 2)

        # Check the animal carousel
        animal_carousel = carousels[0]
        animal_image_containers = animal_carousel.find_elements_by_class_name(
            "image-container"
        )
        self.assertEqual(len(animal_image_containers), 2)
        self.assertEqual(
            animal_image_containers[0]
            .find_elements_by_tag_name("a")[0]
            .get_attribute("href"),
            "http://localhost:3000/animals/1",
        )
        self.assertEqual(
            animal_image_containers[1]
            .find_elements_by_tag_name("a")[0]
            .get_attribute("href"),
            "http://localhost:3000/animals/2",
        )
        animal_descriptions = animal_carousel.find_elements_by_class_name(
            "description-container"
        )
        self.assertEqual(len(animal_descriptions), 2)
        self.assertEqual(
            animal_descriptions[0].find_element_by_tag_name("h4").text, "Heh13"
        )
        self.assertEqual(
            animal_descriptions[0].find_element_by_tag_name("h5").text, "EW"
        )
        next_button = animal_carousel.find_elements_by_class_name("nav")[1]
        next_button.click()
        self.assertEqual(
            animal_descriptions[1].find_element_by_tag_name("h4").text, "Heh13"
        )
        self.assertEqual(
            animal_descriptions[1].find_element_by_tag_name("h5").text, "EW"
        )

        # Check the plant carousel
        plant_carousel = carousels[1]
        plant_image_containers = plant_carousel.find_elements_by_class_name(
            "image-container"
        )
        self.assertEqual(len(plant_image_containers), 2)
        self.assertEqual(
            plant_image_containers[0]
            .find_elements_by_tag_name("a")[0]
            .get_attribute("href"),
            "http://localhost:3000/plants/5",
        )
        self.assertEqual(
            plant_image_containers[1]
            .find_elements_by_tag_name("a")[0]
            .get_attribute("href"),
            "http://localhost:3000/plants/6",
        )
        plant_descriptions = plant_carousel.find_elements_by_class_name(
            "description-container"
        )
        self.assertEqual(len(plant_descriptions), 2)
        self.assertEqual(
            plant_descriptions[0].find_element_by_tag_name("h4").text, "Heh13"
        )
        self.assertEqual(
            plant_descriptions[0].find_element_by_tag_name("h5").text, "EW"
        )
        next_button = plant_carousel.find_elements_by_class_name("nav")[1]
        next_button.click()
        self.assertEqual(
            plant_descriptions[1].find_element_by_tag_name("h4").text, "Heh13"
        )
        self.assertEqual(
            plant_descriptions[1].find_element_by_tag_name("h5").text, "EW"
        )

    def test_animal_link_carousel(self):
        """ Verifies that the animal instance page shows a carousel and that it links correctly. """

        self.driver.get("http://localhost:3000/animals/1")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "organism-name"))
            )
        except TimeoutException:
            self.fail("Instance page failed to load.")

        carousels = self.driver.find_elements_by_class_name("react-slideshow-container")
        self.assertEqual(len(carousels), 1)

        carousel = carousels[0]
        images = carousel.find_elements_by_class_name("image-container")
        self.assertEqual(len(images), 11)

        descriptions = carousel.find_elements_by_class_name("description-container")
        self.assertEqual(len(descriptions), 11)

        images[0].click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/plants/1")

    def test_plant_link_carousel(self):
        """ Verifies that the plant instance page shows a carousel and that it links correctly. """

        self.driver.get("http://localhost:3000/plants/1")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "organism-name"))
            )
        except TimeoutException:
            self.fail("Instance page failed to load.")

        carousels = self.driver.find_elements_by_class_name("react-slideshow-container")
        self.assertEqual(len(carousels), 1)

        carousel = carousels[0]
        images = carousel.find_elements_by_class_name("image-container")
        self.assertEqual(len(images), 10)

        descriptions = carousel.find_elements_by_class_name("description-container")
        self.assertEqual(len(descriptions), 10)

        images[0].click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/animals/1")

    def test_state_graph(self):
        """ Verifies that the state instance page shows a graph for the GAP land. """

        self.driver.get("http://localhost:3000/states/Test%201")
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "loading-finished"))
            )
        except TimeoutException:
            self.fail("Model page failed to load.")

        chart_wrappers = self.driver.find_elements_by_class_name("recharts-wrapper")
        self.assertEqual(len(chart_wrappers), 1)
        chart_surfaces = chart_wrappers[0].find_elements_by_class_name(
            "recharts-pie-sector"
        )
        self.assertEqual(len(chart_surfaces), 4)
        chart_legends = chart_wrappers[0].find_elements_by_class_name(
            "recharts-legend-wrapper"
        )
        self.assertEqual(len(chart_legends), 1)

    def test_splash_home_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_id("title-link")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/")

    def test_splash_animal_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_id("animal-link")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/animals")

    def test_splash_plant_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_id("plant-link")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/plants")

    def test_splash_state_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_id("state-link")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/states")

    def test_splash_about_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_id("about-link")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/about")

    def test_splash_help_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_id("button")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/animals")

    def test_splash_find_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_id("find-button")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/about")

    def test_splash_animal_pic_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_class_name("animal")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/animals")

    def test_splash_plant_pic_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_class_name("leaf")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/plants")

    def test_splash_state_pic_link(self):
        self.driver.get("http://localhost:3000/")
        link = self.driver.find_element_by_class_name("flag")
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:3000/states")

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    print("Starting acceptance tests in Selenium:\n")
    unittest.main(warnings="ignore")
