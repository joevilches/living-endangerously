import flask
from flask_sqlalchemy import SQLAlchemy
import flask_restless
from flask import request
import os
import logging

# Set logging level
log = logging.getLogger("werkzeug")
log.setLevel(logging.ERROR)

# Create the Flask application and the Flask-SQLAlchemy object.
app = flask.Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["DEBUG"] = True
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["DATABASE_URI"]
db = SQLAlchemy(app)
schema_name = os.environ["SCHEMA_NAME"]

# Create the join table for animals and states
animal_state_join = db.Table(
    "animals_territories",
    db.Column(
        "animal_id",
        db.Integer,
        db.ForeignKey(schema_name + ".animals.animal_id"),
        primary_key=True,
    ),
    db.Column(
        "territory_code",
        db.Unicode,
        db.ForeignKey(schema_name + ".states.code"),
        primary_key=True,
    ),
    schema=schema_name,
)

# Create the join table for plants and states
plant_state_join = db.Table(
    "plants_territories",
    db.Column(
        "plant_id",
        db.Integer,
        db.ForeignKey(schema_name + ".plants.plant_id"),
        primary_key=True,
    ),
    db.Column(
        "territory_code",
        db.Unicode,
        db.ForeignKey(schema_name + ".states.code"),
        primary_key=True,
    ),
    schema=schema_name,
)

animal_habitat_join = db.Table(
    "animals_habitats",
    db.Column(
        "animal_id",
        db.Integer,
        db.ForeignKey(schema_name + ".animals.animal_id"),
        primary_key=True,
    ),
    db.Column(
        "habitat_name",
        db.Unicode,
        db.ForeignKey(schema_name + ".habitats.habitat_name"),
        primary_key=True,
    ),
    schema=schema_name,
)

plant_habitat_join = db.Table(
    "plants_habitats",
    db.Column(
        "plant_id",
        db.Integer,
        db.ForeignKey(schema_name + ".plants.plant_id"),
        primary_key=True,
    ),
    db.Column(
        "habitat_name",
        db.Unicode,
        db.ForeignKey(schema_name + ".habitats.habitat_name"),
        primary_key=True,
    ),
    schema=schema_name,
)

# Populates the state object's animal and plant relations
def territory_post_get_single(result=None, search_params=None, **kw):
    plants = (
        Plant.query.filter(Plant.states.any(State.code == result["code"]))
        .limit(20)
        .all()
    )
    result["plants"] = list(
        map(
            lambda plant: {
                "id": plant.plant_id,
                "image_link": plant.image_link,
                "common_name": plant.common_name,
                "category": plant.category,
                "description": plant.description,
            },
            plants,
        )
    )
    animals = (
        Animal.query.filter(Animal.states.any(State.code == result["code"]))
        .limit(20)
        .all()
    )
    result["animals"] = list(
        map(
            lambda animal: {
                "id": animal.animal_id,
                "image_link": animal.image_link,
                "common_name": animal.common_name,
                "category": animal.category,
                "description": animal.description,
            },
            animals,
        )
    )


# Populates either the organism's plant or animal relation
def organism_post_get_single(result=None, search_params=None, **kw):
    if "tax_kingdom" in result and result["tax_kingdom"] == "Animalia":
        plants = Plant.query.filter_by(category=result["category"]).limit(20).all()
        result["plants"] = list(
            map(
                lambda plant: {
                    "id": plant.plant_id,
                    "image_link": plant.image_link,
                    "common_name": plant.common_name,
                    "category": plant.category,
                    "description": plant.description,
                },
                plants,
            )
        )
    elif "tax_kingdom" in result:
        animals = Animal.query.filter_by(category=result["category"]).limit(20).all()
        result["animals"] = list(
            map(
                lambda animal: {
                    "id": animal.animal_id,
                    "image_link": animal.image_link,
                    "common_name": animal.common_name,
                    "category": animal.category,
                    "description": animal.description,
                },
                animals,
            )
        )


def organism_post_get_many(result=None, search_params=None, **kw):
    # print(result)
    for organism in result["objects"]:
        for state in organism["states"]:
            code = state["code"]
            state.clear()
            state["code"] = code


# Allows cross-origin requests
def add_cors_header(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Methods"] = "GET"
    response.headers[
        "Access-Control-Allow-Headers"
    ] = "Origin, X-Requested-With, Content-Type, Accept"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    return response


# Create your Flask-SQLALchemy models as usual but with the following two
# (reasonable) restrictions:
#   1. They must have a primary key column of type sqlalchemy.Integer or
#      type sqlalchemy.Unicode.
#   2. They must have an __init__ method which accepts keyword arguments for
#      all columns (the constructor in flask.ext.sqlalchemy.SQLAlchemy.Model
#      supplies such a method, so you don't need to declare a new one).
class Animal(db.Model):
    __tablename__ = "animals"
    __table_args__ = {"schema": schema_name}
    animal_id = db.Column(db.Integer, primary_key=True)
    sci_name = db.Column(db.Unicode, unique=True)
    tax_kingdom = db.Column(db.Unicode)
    tax_phylum = db.Column(db.Unicode)
    tax_class = db.Column(db.Unicode)
    tax_order = db.Column(db.Unicode)
    tax_family = db.Column(db.Unicode)
    tax_genus = db.Column(db.Unicode)
    common_name = db.Column(db.Unicode)
    category = db.Column(db.Unicode)
    image_link = db.Column(db.Unicode)
    category_reason = db.Column(db.UnicodeText)
    category_comment = db.Column(db.UnicodeText)
    threats_notes = db.Column(db.UnicodeText)
    description = db.Column(db.UnicodeText)
    states = db.relationship(
        "State",
        secondary=animal_state_join,
        lazy="subquery",
        backref=db.backref("animals", lazy=True),
    )
    habitats = db.relationship(
        "Habitat",
        secondary=animal_habitat_join,
        lazy="subquery",
        backref=db.backref("animals", lazy=True),
    )


class Plant(db.Model):
    __tablename__ = "plants"
    __table_args__ = {"schema": schema_name}
    plant_id = db.Column(db.Integer, primary_key=True)
    sci_name = db.Column(db.Unicode, unique=True)
    tax_kingdom = db.Column(db.Unicode)
    tax_phylum = db.Column(db.Unicode)
    tax_class = db.Column(db.Unicode)
    tax_order = db.Column(db.Unicode)
    tax_family = db.Column(db.Unicode)
    tax_genus = db.Column(db.Unicode)
    common_name = db.Column(db.Unicode)
    category = db.Column(db.Unicode)
    pop_trend = db.Column(db.Unicode)
    image_link = db.Column(db.Unicode)
    category_reason = db.Column(db.UnicodeText)
    category_comment = db.Column(db.UnicodeText)
    threats_notes = db.Column(db.UnicodeText)
    description = db.Column(db.UnicodeText)
    states = db.relationship(
        "State",
        secondary=plant_state_join,
        lazy="subquery",
        backref=db.backref("plants", lazy=True),
    )
    habitats = db.relationship(
        "Habitat",
        secondary=plant_habitat_join,
        lazy="subquery",
        backref=db.backref("plants", lazy=True),
    )


class State(db.Model):
    __tablename__ = "states"
    __table_args__ = {"schema": schema_name}
    code = db.Column(db.Unicode, primary_key=True)
    name = db.Column(db.Unicode, unique=True, nullable=False)
    human_pop = db.Column(db.Unicode, nullable=False)
    land_area = db.Column(db.Unicode, nullable=False)
    gap1 = db.Column(db.Unicode, nullable=False)
    gap2 = db.Column(db.Unicode, nullable=False)
    gap3 = db.Column(db.Unicode, nullable=False)
    unprotected = db.Column(db.Unicode, nullable=False)
    flag_image = db.Column(db.Unicode)
    map_image = db.Column(db.Unicode, nullable=False)
    human_pop_int = db.Column(db.Integer, nullable=False)
    gap1_int = db.Column(db.Integer, nullable=False)
    gap2_int = db.Column(db.Integer, nullable=False)
    gap3_int = db.Column(db.Integer, nullable=False)
    unprotected_int = db.Column(db.Integer, nullable=False)
    land_area_int = db.Column(db.Integer, nullable=False)
    # Animal Categories
    total_animals = db.Column(db.Integer, nullable=False)
    extinct_wild_animals = db.Column(db.Integer, nullable=False)
    vulnerable_animals = db.Column(db.Integer, nullable=False)
    imperiled_animals = db.Column(db.Integer, nullable=False)
    scared_animals = db.Column(db.Integer, nullable=False)
    unrankable_animals = db.Column(db.Integer, nullable=False)
    not_ranked_animals = db.Column(db.Integer, nullable=False)
    possibly_extinct_animals = db.Column(db.Integer, nullable=False)
    critically_imperiled_animals = db.Column(db.Integer, nullable=False)
    presumed_extinct_animals = db.Column(db.Integer, nullable=False)
    apparently_secure_animals = db.Column(db.Integer, nullable=False)
    secure_animals = db.Column(db.Integer, nullable=False)
    not_applicable_animals = db.Column(db.Integer, nullable=False)
    # Plant Categories
    total_plants = db.Column(db.Integer, nullable=False)
    extinct_wild_plants = db.Column(db.Integer, nullable=False)
    vulnerable_plants = db.Column(db.Integer, nullable=False)
    imperiled_plants = db.Column(db.Integer, nullable=False)
    scared_plants = db.Column(db.Integer, nullable=False)
    unrankable_plants = db.Column(db.Integer, nullable=False)
    not_ranked_plants = db.Column(db.Integer, nullable=False)
    possibly_extinct_plants = db.Column(db.Integer, nullable=False)
    critically_imperiled_plants = db.Column(db.Integer, nullable=False)
    presumed_extinct_plants = db.Column(db.Integer, nullable=False)
    apparently_secure_plants = db.Column(db.Integer, nullable=False)
    secure_plants = db.Column(db.Integer, nullable=False)
    not_applicable_plants = db.Column(db.Integer, nullable=False)


class Habitat(db.Model):
    __tablename__ = "habitats"
    __table_args__ = {"schema": schema_name}
    habitat_name = db.Column(db.Unicode, primary_key=True)


# Create the database tables.
db.create_all()

# Create the Flask-Restless API manager.
manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
manager.create_api(
    Animal,
    methods=["GET"],
    max_results_per_page=100,
    postprocessors={
        "GET_SINGLE": [organism_post_get_single],
        "GET_MANY": [organism_post_get_many],
    },
)
manager.create_api(
    Plant,
    methods=["GET"],
    max_results_per_page=100,
    postprocessors={"GET_SINGLE": [organism_post_get_single]},
)
manager.create_api(
    State,
    methods=["GET"],
    max_results_per_page=50,
    postprocessors={"GET_SINGLE": [territory_post_get_single]},
    exclude_columns=["animals", "plants"],
)

# Adds cors headers to every response
app.after_request(add_cors_header)


# start the flask loop
if __name__ == "__main__":
    app.run()
