import requests
import psycopg2
from psycopg2.extensions import AsIs


class animal_database:
    def __init__(self, host):
        self.host = host

    def connect(self):
        self.conn = psycopg2.connect(self.host)
        self.cur = self.conn.cursor()

    def disconnect(self):
        self.cur.close()
        self.conn.close()

    def write(self, info):
        cur = self.cur
        conn = self.conn

        animals_info = self.get_animal_table_data(info)
        self.write_to_animals(animals_info, cur)

        hab = {}
        terr = {}
        if "animal_id" in info:
            hab["animal_id"] = info["animal_id"]
            terr["animal_id"] = info["animal_id"]
        else:
            hab["plant_id"] = info["plant_id"]
            terr["plant_id"] = info["plant_id"]
        hab["habitats"] = info["habitats"]
        terr["territories"] = info["states"]

        self.write_to_habitats(hab, cur)
        self.write_to_territories(terr, cur)

    def write_to_habitats(self, info, cur):
        if info["habitats"] != None:
            id = ""
            sql = ""
            if "animal_id" in info:
                id = info["animal_id"]
                sql = "INSERT INTO endangerous.animals_habitats (animal_id, habitat_name) VALUES (%s, %s)"
            else:
                id = info["plant_id"]
                sql = "INSERT INTO endangerous.plants_habitats (plant_id, habitat_name) VALUES (%s, %s)"

            hab_set = set()
            for hab in info["habitats"]:
                hab_set.add(hab)

            for hab in hab_set:
                cur.execute(sql, (id, hab))

            self.conn.commit()

    def write_to_territories(self, info, cur):
        if info["territories"] != None:
            id = ""
            sql = ""
            if "animal_id" in info:
                id = info["animal_id"]
                sql = "INSERT INTO endangerous.animals_territories (animal_id, territory_code) VALUES (%s, %s)"
            else:
                id = info["plant_id"]
                sql = "INSERT INTO endangerous.plants_territories (plant_id, territory_code) VALUES (%s, %s)"

            for terr in info["territories"]:
                cur.execute(sql, (id, terr))

            self.conn.commit()

    def write_to_animals(self, info, cur):
        sql = ""
        if "animal_id" in info:
            sql = "INSERT INTO endangerous.animals (%s) VALUES %s"
        else:
            sql = "INSERT INTO endangerous.plants (%s) VALUES %s"
        columns = info.keys()
        values = [info[column] for column in columns]
        sql = cur.mogrify(sql, (AsIs(",".join(columns)), tuple(values)))
        # print sql
        cur.execute(sql)
        self.conn.commit()

    def get_animal_table_data(self, info):
        result = {}
        for key in info:
            if key != "states" and key != "provinces" and key != "habitats":
                result[key] = info[key]
        return result
