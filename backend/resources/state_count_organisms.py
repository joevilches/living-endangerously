import requests
import urllib
import psycopg2

conn = None

categories = [
    "Extinct in the Wild",
    "Vulnerable",
    "Imperiled",
    "A little scared",
    "Unrankable",
    "Not Yet Ranked",
    "Possibly Extinct",
    "Critically Imperiled",
    "Presumed Extinct",
    "Apparently Secure",
    "Secure",
    "Not Applicable",
]

animal_column_names = [
    "extinct_wild_animals",
    "vulnerable_animals",
    "imperiled_animals",
    "scared_animals",
    "unrankable_animals",
    "not_ranked_animals",
    "possibly_extinct_animals",
    "critically_imperiled_animals",
    "presumed_extinct_animals",
    "apparently_secure_animals",
    "secure_animals",
    "not_applicable_animals",
]

plant_column_names = [
    "extinct_wild_plants",
    "vulnerable_plants",
    "imperiled_plants",
    "scared_plants",
    "unrankable_plants",
    "not_ranked_plants",
    "possibly_extinct_plants",
    "critically_imperiled_plants",
    "presumed_extinct_plants",
    "apparently_secure_plants",
    "secure_plants",
    "not_applicable_plants",
]


def getStates(cursor):
    sql = "SELECT * FROM endangerous.states"
    cursor.execute(sql)
    return cursor.fetchmany(50)


def getTotalAnimals(cursor, code):
    sql = "SELECT count(distinct animal_id) FROM endangerous.animals_territories WHERE territory_code=%s"
    cursor.execute(sql, (code,))
    return cursor.fetchone()[0]


def getTotalPlants(cursor, code):
    sql = "SELECT count(distinct plant_id) FROM endangerous.plants_territories WHERE territory_code=%s"
    cursor.execute(sql, (code,))
    return cursor.fetchone()[0]


def getAnimalsWithCategory(cursor, code, category):
    sql = "SELECT count(distinct animal_id) FROM endangerous.animals_territories as R WHERE territory_code=%s\
           AND (SELECT distinct category FROM endangerous.animals as L WHERE L.animal_id=R.animal_id)=%s"
    cursor.execute(sql, (code, category))
    return cursor.fetchone()[0]


def getPlantsWithCategory(cursor, code, category):
    sql = "SELECT count(distinct plant_id) FROM endangerous.plants_territories as R WHERE territory_code=%s\
           AND (SELECT distinct category FROM endangerous.plants as L WHERE L.plant_id=R.plant_id)=%s"
    # print(cursor.mogrify(sql, (code, category)))
    cursor.execute(sql, (code, category))
    return cursor.fetchone()[0]


def setCountForCategory(cursor, code, column_name, count):
    sql = "UPDATE endangerous.states SET " + column_name + "=%s WHERE code=%s"
    # print(cursor.mogrify(sql, (count, code)))
    cursor.execute(sql, (count, code))


def setTotalAnimals(cursor, total, code):
    sql = "UPDATE endangerous.states SET total_animals=%s WHERE code=%s"
    cursor.execute(sql, (total, code))


def setTotalPlants(cursor, total, code):
    sql = "UPDATE endangerous.states SET total_plants=%s WHERE code=%s"
    cursor.execute(sql, (total, code))


def main():
    try:
        conn = psycopg2.connect(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        cursor = conn.cursor()
        states = getStates(cursor)
        for state in states:
            code = state[9]
            print(code)
            total_animals = getTotalAnimals(cursor, code)
            setTotalAnimals(cursor, total_animals, code)
            total_plants = getTotalPlants(cursor, code)
            setTotalPlants(cursor, total_plants, code)
            sum_animals = 0
            sum_plants = 0
            # print("Total Animals:", end=" ")
            # print(total_animals, end=", ")
            # print("Total Plants:", end=" ")
            # print(total_plants)
            for category, animal_column_name, plant_column_name in zip(
                categories, animal_column_names, plant_column_names
            ):
                animals_with_category = getAnimalsWithCategory(cursor, code, category)
                sum_animals += animals_with_category
                setCountForCategory(
                    cursor, code, animal_column_name, animals_with_category
                )

                plants_with_category = getPlantsWithCategory(cursor, code, category)
                # print(category + ": " + str(plants_with_category))
                sum_plants += plants_with_category
                setCountForCategory(
                    cursor, code, plant_column_name, plants_with_category
                )

            # print("Sum animals:", end=" ")
            # print(sum_animals, end=", ")
            # print("Sum plants:", end=" ")
            # print(sum_plants)
            assert sum_animals == total_animals
            assert sum_plants == total_plants

        conn.commit()
        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == "__main__":
    main()
