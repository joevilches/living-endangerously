import requests
import urllib
import psycopg2

conn = None


def setIntegers(cursor, name, population, land_area, gap1, gap2, gap3, unprotected):
    sql = "UPDATE endangerous.states set human_pop_int=%s, land_area_int=%s, gap1_int=%s, gap2_int=%s, gap3_int=%s, unprotected_int=%s WHERE name=%s"
    cursor.execute(sql, (population, land_area, gap1, gap2, gap3, unprotected, name))


def getStates(cursor):
    sql = "SELECT * FROM endangerous.states"
    cursor.execute(sql)
    return cursor.fetchmany(50)


def getGap(gap_str):
    if gap_str == "< 1":
        return 0
    return int(gap_str, 10)


def main():
    try:
        conn = psycopg2.connect(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        cursor = conn.cursor()
        states = getStates(cursor)
        # print(states)
        for state in states:
            human_pop = int(state[0], 10)
            land_area = int(state[1].replace(",", ""), 10)
            gap1 = getGap(state[2])
            gap2 = getGap(state[3])
            gap3 = getGap(state[4])
            unprotected = getGap(state[5])
            name = state[6]
            setIntegers(
                cursor, name, human_pop, land_area, gap1, gap2, gap3, unprotected
            )

        conn.commit()
        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == "__main__":
    main()
