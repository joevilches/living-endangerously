import requests
import re
import json
import psycopg2
from psycopg2.extensions import AsIs


def get_id(json, name):
    for result in json["results"]:
        if result["title"] == name:
            return result["id"]
    return None


def get_picture(json):
    return json["taxonConcept"]["dataObjects"][0]["mediaURL"]


def write_to_db(links):
    print("Writing to database")
    host_name = "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
    conn = psycopg2.connect(host_name)
    cur = conn.cursor()

    for l in links:
        print(l)
        print(l[1])
        link = l[1].replace("'", "''")
        sql = (
            "UPDATE endangerous.animals SET image_link='"
            + link
            + "' WHERE sci_name='"
            + l[0]
            + "'"
        )
        cur.execute(sql)

    conn.commit()
    cur.close()
    conn.close()


name_search_url = "https://eol.org/api/search/1.0.json"
picture_url = "https://eol.org/api/pages/1.0?details=true&images_per_page=10"
files = ["./mammals.txt", "./plants.txt"]

pics = []
no_pics = []
for file_name in files:
    f = open(file_name)
    for name in f:
        name = name[0 : len(name) - 1]
        print("TRYING :: " + name)
        id_resp = requests.get(name_search_url + "?q=" + name)
        json_resp = json.loads(id_resp.text)
        id = get_id(json_resp, name)

        if id != None:
            try:
                pic_resp = requests.get(picture_url + "&id=" + str(id))
                json_pic = json.loads(pic_resp.text)
                link = get_picture(json_pic)

                tuple_thing = (name, link)
                pics.append(tuple_thing)
                print("SUCCESS :: " + name + "\n")
            except:
                no_pics.append(name)
                print("PICTURE NOT FOUND :: " + name + "\n")
        else:
            no_pics.append(name)
            print("PICTURE NOT FOUND :: " + name + "\n")

    write_to_db(pics)

print(link)
