import requests
import json


def create_states():
    url = "http://127.0.0.1:5000/api/states"
    headers = {"Content-Type": "application/json"}
    for i in range(0, 19):
        data = {
            "name": "Test " + str(i),
            "code": "Test " + str(i),
            "gap1": str(i),
            "gap1_int": i,
            "gap2": str(i + 10),
            "gap2_int": i + 10,
            "gap3": str(i + 20),
            "gap3_int": i + 20,
            "unprotected": str(i + 30),
            "unprotected_int": i + 30,
            "human_pop": str(10000 - i),
            "human_pop_int": 10000 - i,
            "land_area": str(20000 * i),
            "land_area_int": 20000 * i,
            "map_image": "../../../florida.png",
        }
        response = requests.post(url, json=data, headers=headers)
        assert response.status_code == 201


def create_animals():
    url = "http://127.0.0.1:5000/api/animals"
    headers = {"Content-Type": "application/json"}
    for i in range(0, 10):
        states = []
        if i == 0 or i == 1:
            states.append({"code": "Test 13"})

        data = {
            "animal_id": i + 1,
            "category": "EW",
            "common_name": "Heh13",
            "image_link": "../../../jackstenglein.jpg",
            "sci_name": "Test " + str(i),
            "tax_kingdom": "Animalia",
            "tax_class": "Mammalia",
            "tax_family": "Hominidae",
            "tax_genus": "Homo",
            "tax_order": "Primates",
            "tax_phylum": "Chordata",
            "states": states,
        }
        response = requests.post(url, json=data, headers=headers)
        assert response.status_code == 201


def create_plants():
    url = "http://127.0.0.1:5000/api/plants"
    headers = {"Content-Type": "application/json"}
    for i in range(0, 11):
        states = []
        if i == 4 or i == 5:
            states.append({"code": "Test 13"})
            states.append({"code": "Test 14"})

        data = {
            "plant_id": i + 1,
            "category": "EW",
            "common_name": "Heh13",
            "image_link": "../../../jackstenglein.jpg",
            "sci_name": "Test " + str(i),
            "tax_kingdom": "Plantae",
            "tax_class": "Mammalia",
            "tax_family": "Hominidae",
            "tax_genus": "Homo",
            "tax_order": "Primates",
            "tax_phylum": "Chordata",
            "states": states,
        }
        response = requests.post(url, json=data, headers=headers)
        assert response.status_code == 201


def main():
    print("Creating states...")
    create_states()
    print("Finished creating states.")
    print("Creating animals...")
    create_animals()
    print("Finished creating animals.")
    print("Creating plants...")
    create_plants()
    print("Finished creating plants.")


if __name__ == "__main__":
    main()
