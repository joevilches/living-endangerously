from unittest import main, TestCase
import xml.etree.ElementTree as ET

from animal_handler import animal_handler
from animal_main import extract_list
from animal_database import animal_database

import unittest
import psycopg2

from state_fetch import executeQuery, buildUrl
from state_update_ints import getGap, getStates


class Python_TestCase(TestCase):
    def test_getGapZero(self):
        result = getGap("< 1")
        self.assertEqual(result, 0)

    def test_getGapGen(self):
        result = getGap("5")
        self.assertEqual(result, 5)

    def test_getAnimalTableData(self):
        info = {"animals": 1}
        adb = animal_database(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        result = adb.get_animal_table_data(info)
        self.assertEqual(result["animals"], 1)

    def test_getAnimalTableData2(self):
        info = {"animals": 1, "test": 3}
        adb = animal_database(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        result = adb.get_animal_table_data(info)
        self.assertTrue(result["animals"] == 1 and result["test"] == 3)

    def test_extract_empty_territory(self):
        XML_tree = ET.parse("test_xml_empty.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.extract_territories(root)
        expected = []
        self.assertListEqual(actual, expected)

    def test_extract_territories(self):
        XML_tree = ET.parse("test_territory.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.extract_territories(root)
        expected = ["AL", "TX", "OK"]
        self.assertListEqual(actual, expected)

    def test_excessive_extract_territories(self):
        XML_tree = ET.parse("excessive_info_habitats.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.extract_territories(root)
        expected = []
        self.assertListEqual(actual, expected)

    def test_extract_excessive_habitats(self):
        XML_tree = ET.parse("excessive_info_habitats.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.extract_habitats(root)
        expected = []
        self.assertListEqual(actual, expected)

    def test_extract_empty_habitats(self):
        XML_tree = ET.parse("test_xml_empty.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.extract_habitats(root)
        expected = []
        self.assertListEqual(actual, expected)

    def test_extract_habitats(self):
        XML_tree = ET.parse("test_habits.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.extract_habitats(root)
        expected = [
            "Herbaceous wetland",
            "Riparian",
            "Woodland - Conifer",
            "Woodland - Hardwood",
            "Woodland - Mixed",
        ]
        self.assertListEqual(actual, expected)

    def test_child_dict(self):
        XML_tree = ET.parse("test_habits.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.get_child_dict(root)
        self.assertEqual(len(actual), 3)

    def test_child_dict2(self):
        XML_tree = ET.parse("test_xml_empty.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.get_child_dict(root)
        self.assertEqual(len(actual), 2)

    def test_parse_empty(self):
        XML_tree = ET.parse("test_xml_empty.xml")
        root = XML_tree.getroot()
        animal_handle = animal_handler()

        actual = animal_handle.parse_response(root)
        self.assertEqual(len(actual), 0)

    def test_extract_list(self):
        actual = extract_list("empty.txt")
        expected = []
        self.assertListEqual(actual, expected)

    def test_extract_lists(self):
        actual = extract_list("test_mammals.txt")
        expected = [
            "Sciurus arizonensis",
            "Sciurus carolinensis",
            "Sciurus griseus",
            "Sciurus nayaritensis",
            "Sciurus niger",
        ]
        self.assertListEqual(actual, expected)

    def test_url(self):
        """
        Test that it builds the correct url
        """
        result = buildUrl(3)
        expected_result = "https://api.census.gov/data/2017/pep/population?get=POP,GEONAME&DATE=9&for=state:3"
        self.assertEqual(result, expected_result)

    def test_url2(self):
        """
        Test that it builds the correct url
        """
        result = buildUrl(12)
        expected_result = "https://api.census.gov/data/2017/pep/population?get=POP,GEONAME&DATE=9&for=state:12"
        self.assertEqual(result, expected_result)

    def test_executeQuery(self):
        conn = psycopg2.connect(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        cur = conn.cursor()
        expected_result = (
            b"UPDATE endangerous.states set human_pop='pop' WHERE name='Texas'"
        )
        result = executeQuery(cur, "Texas", "pop")
        self.assertEqual(result, expected_result)
        cur.close()
        conn.close()
        "UPDATE endangerous.states set human_pop='pop' WHERE name='Texas'"
        "UPDATE endangerous.states set human_pop='pop' WHERE name='Texas'"

    def test_executeQuery2(self):
        conn = psycopg2.connect(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        cur = conn.cursor()
        expected_result = (
            b"UPDATE endangerous.states set human_pop='pop' WHERE name='Utah'"
        )
        result = executeQuery(cur, "Utah", "pop")
        self.assertEqual(result, expected_result)
        cur.close()
        conn.close()
        "UPDATE endangerous.states set human_pop='pop' WHERE name='Utah'"
        "UPDATE endangerous.states set human_pop='pop' WHERE name='Utah'"

    def test_getStates(self):
        conn = psycopg2.connect(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        cur = conn.cursor()
        result = getStates(cur)
        self.assertEqual(len(result), 50)
        cur.close()
        conn.close()


if __name__ == "__main__":
    print("Starting unit tests for Python code")
    main()
