import unittest
import psycopg2
import sys

sys.path.insert(0, "/resources")

from resources.state_fetch import executeQuery, buildUrl


class TestFetch(unittest.TestCase):
    def test_url(self):
        """
        Test that it builds the correct url
        """
        result = buildUrl(3)
        expected_result = "https://api.census.gov/data/2017/pep/population?get=POP,GEONAME&DATE=9&for=state:3"
        self.assertEqual(result, expected_result)

    def test_executeQuery(self):
        conn = psycopg2.connect(
            "host=endangerousdb.cvmgvbnho3kr.us-west-2.rds.amazonaws.com dbname=endangerousDB user=master password=3l3ph&nt!"
        )
        cur = conn.cursor()
        expected_result = (
            b"UPDATE endangerous.states set human_pop='pop' WHERE name='Texas'"
        )
        result = executeQuery(cur, "Texas", "pop")
        self.assertEqual(result, expected_result)
        cur.close()
        conn.close()
        "UPDATE endangerous.states set human_pop='pop' WHERE name='Texas'"
        "UPDATE endangerous.states set human_pop='pop' WHERE name='Texas'"


if __name__ == "__main__":
    unittest.main()
